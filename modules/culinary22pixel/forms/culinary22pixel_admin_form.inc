<?php

/**
 * @file
 * Contains the module settings form.
 */

/**
 * Amazon pixel settings form.
 */
function culinary22pixel_admin_form() {
  $form = [];
  $form[] = [
    '#type' => 'item',
    '#markup' => t('This form contains settings for Culinary 2022 pixel on this website.'),
  ];
  $form['culinary22pixel_pages'] = [
    '#type' => 'textarea',
    '#title' => t('Culinary 2022 pixel pages'),
    '#default_value' => variable_get('culinary22pixel_pages', NULL),
    '#rows' => 10,
    '#description' => t('Enter the Culinary 2022 pixel pages.'),
  ];

  return system_settings_form($form);
}
