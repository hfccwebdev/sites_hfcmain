<?php

/**
 * @file
 * Contains the module settings form.
 */

/**
 * Amazon pixel settings form.
 */
function amazonpixel_admin_form() {
  $form = [];
  $form[] = [
    '#type' => 'item',
    '#markup' => t('This form contains settings for Amazon pixel on this website.'),
  ];
  $form['amazonpixel_pages'] = [
    '#type' => 'textarea',
    '#title' => t('Amazon pixel pages'),
    '#default_value' => variable_get('amazonpixel_pages', NULL),
    '#rows' => 10,
    '#description' => t('Enter the Amazon pixel pages.'),
  ];

  return system_settings_form($form);
}
