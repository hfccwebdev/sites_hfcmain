<?php

/**
 * Defines data integrity tools for this site.
 */
class SiteDataIntegrityTools {

  /**
   * Create an instance of this class.
   */
  public static function create() {
    return new static();
  }

  /**
   * Check for missing Programs.
   */
  public function missingProgramsCheck() {

    $options = WebServicesClient::getProgramsOpts();
    $programs = [];

    $result = db_query("SELECT nid FROM {node} WHERE type='program' AND status='1' ORDER BY 'title'")->fetchAll();

    $output = [t("### Missing Catalog Programs"), ""];


    foreach ($result as $item) {
      $node = node_load($item->nid);
      if (!empty($node->field_catalog_programs[LANGUAGE_NONE])) {
        foreach ($node->field_catalog_programs[LANGUAGE_NONE] as $program) {
          $id = $program['value'];
          $programs[$id] = TRUE;
          if (empty($options[$id])) {
            $values = ['@nid' => url('node/' . $node->nid, ['absolute' => TRUE]), '@title' => $node->title, '@program' => $id];
            $output[] = t("* [@title](@nid) missing program @program", $values);
          }
        }
      }
    }

    return PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * Check for missing courses.
   */
  public function missingCoursesCheck() {

    $output = [t("### Missing Catalog Courses"), ""];

    $options = WebServicesClient::getCoursesOpts();
    $nids = db_query("SELECT nid FROM {node} WHERE type='course' AND status='1' ORDER BY 'title'")->fetchAll();

    foreach ($nids as $item) {
      $node = node_load($item->nid);
      if (!empty($node->field_course_description[LANGUAGE_NONE])) {
        foreach ($node->field_course_description[LANGUAGE_NONE] as $course) {
          $id = $course['course_number'];
          if (empty($options[$id])) {
            $values = ['@nid' => url('node/' . $node->nid, ['absolute' => TRUE]), '@title' => $node->title, '@course' => $id];
            $output[] = t("* [@title](@nid) missing course @course", $values);
          }
        }
      }
    }
    return PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * Summary Check of employee info for reports.
   */
  public function employeeMismatchReport() {
    $changes = $this->scanEmployeeInfo();
    $site_name = variable_get('site_name');
    $output = ["Checking Staff Directory info changes for employee content for $site_name.", ""];

    // We only care about certain differences for this report.
    // The admin/content/people page will show all changes.
    $found = 0;
    foreach ($changes as $change) {
      $differences = [];

      if ($change['node']->title !== $change['clone']->title) {
        $differences[] = 'name';
      }

      foreach (['hank_id', 'firstname', 'lastname', 'suffix'] as $field) {
        if ($change['node']->{"field_person_$field"} !== $change['clone']->{"field_person_$field"}) {
          $differences[] = $field;
        }
      }

      if ([] !== $differences) {
        $id = !empty($change['node']->field_person_hank_id[LANGUAGE_NONE])
          ? $change['node']->field_person_hank_id[LANGUAGE_NONE][0]['value']
          : 0;

        $values = [
          '@title' => $change['clone']->title,
          '@id' => str_pad($id, 7, '0', STR_PAD_LEFT),
          '@fields' => implode(', ', $differences),
        ];
        $output[] = t('* @id @title changes to @fields', $values);
        $found++;
      }
    }
    $output[] =t('Changes found: @found', ['@found' => $found]);

    return implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * Check for outdated employee content.
   */
  public function scanEmployeeInfo() {
    $nids = db_query("SELECT nid from {node} WHERE type='faculty' AND status='1' ORDER BY title")->fetchCol();
    $check = array_map('self::checkEmployeeData', $nids);
    $filtered = array_filter($check);
    return $filtered;
  }

  /**
   * Callback to check each employee for update.
   */
  private function checkEmployeeData($nid) {
    $node = node_load($nid);
    $clone = clone $node;
    if (MysiteTools::create()->updateEmployeeHankFields($clone)) {
      return ['node' => $node, 'clone' => $clone];
    }
  }
}
