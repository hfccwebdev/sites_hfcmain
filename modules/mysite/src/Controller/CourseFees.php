<?php

/**
 * Module page callbacks.
 */
class CourseFees {

  public static function view($term) {
    if (in_array(drupal_strtoupper($term), ['WI', 'SU', 'FA'])) {
      $hankterm = self::getAppropriateSemester(drupal_strtoupper($term));
      $term_id = preg_replace("|/|", "", $hankterm["terms_id"]);
      $results = HfcHankApi::getData('course-sections-fees', $term_id, [], TRUE, NULL, 30) ?: [];
      drupal_set_title(t('@term Course Fee Schedule', ['@term' => $hankterm['term_desc']]));
      $rows = [];
      foreach ($results as $result) {
        if (!empty($result->COURSE_FEE) || !empty($result->EXCESS_CONTACT_HOURS_FEE_ID) || !empty($result->EXCESS_TUITION_FEE) || !empty($result->BOOK_FEE)) {
          $rows[] = [
            $result->SEC_SUBJECT . '-' . $result->SEC_COURSE_NO . '-' . $result->SEC_NO,
            $result->SEC_SHORT_TITLE,
            ['data' => number_format($result->COURSE_FEE, 2, '.', ','), 'class' => ['course-fee']],
            ['data' => number_format($result->EXCESS_CONTACT_HOURS_FEE_ID, 2, '.', ','), 'class' => ['course-fee']],
            ['data' => number_format($result->EXCESS_CONTACT_HOURS_FEE_OD, 2, '.', ','), 'class' => ['course-fee']],
            ['data' => number_format($result->EXCESS_TUITION_FEE, 2, '.', ','), 'class' => ['course-fee']],
            ['data' => number_format($result->BOOK_FEE, 2, '.', ','), 'class' => ['course-fee']],
          ];
        }
      }
      return [
        [
          '#markup' => t('<p>For an explanation of all of our fees, see
            <a href="/tuition-and-payment/fee-information">HFC fee definitions and explanations.</a></p>'),
        ],
        [
          '#theme' => 'table',
          '#header' => [
            t('Course Name & Number'),
            t('Course Name'),
            t('Course Fee'),
            t('In District<br>Excess Contact Hour Fee'),
            t('Out of District<br>Excess Contact Hour Fee'),
            t('Excess Tuition Fee'),
            t('Book Fee'),
          ],
          '#rows' => $rows,
          '#empty' => t('Fees for this term are not currently available.'),
        ],
      ];
    }
    else {
      drupal_not_found();
    }
  }

  private static function getAppropriateSemester($term) {
    $year = date('y');
    $terms = WebServicesClient::getTerms();
    if($terms["$year/$term"]['term_end_date'] < REQUEST_TIME) {
      $year += 1;
    }
    return $terms["$year/$term"];
  }
}
