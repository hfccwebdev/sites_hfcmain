<?php

/**
 * Module page callbacks.
 */
class MysitePages {

  /**
   * Custom callback for /node.
   */
  public static function defaultPage() {
    drupal_set_title('');
    return [];
  }

  /**
   * Custom callback for /error403.
   */
  public static function error403() {
    drupal_set_title('Access denied');
    $output = [['#markup' => t('You are not authorized to access this page.')]];

    global $user;
    if ($user->uid == 0) {
      $options = ['attributes' => ['class' => ['hfc-button', 'hfc-button-primary']]];

      if ($destination = drupal_get_destination()) {
        $options['query'] = $destination;
      }

      $output[] = [
        '#prefix' => '<p>',
        '#markup' => l('Log In', 'user/login', $options),
        '#suffix' => '</p>',
      ];
    }
    return $output;
  }

  /**
   * Custom callback for /daily_crime_log.
   */
  public static function dailyCrimeLog() {
    drupal_set_title('Daily Crime Log');

    $query = db_select('node', 'n')
      ->condition('n.type', 'daily_crime_log')
      ->condition('n.status', NODE_PUBLISHED)
      ->orderBy('n.created', 'DESC')
      ->range(0, 1);
    $query->leftJoin('field_data_field_attachments', 'a', "a.revision_id = n.vid AND a.entity_type = 'node'");
    $query->leftJoin('file_managed', 'f', "f.fid = a.field_attachments_fid");
    $query->addField('f', 'uri');
    $result = $query->execute()->fetchCol();

    if ($result) {
      $result = reset($result);

      $url = file_create_url($result);

      drupal_goto($url);
      drupal_exit();
    }
    else {
      return t("Could not load Daily Crime Log");
    }
  }

}
