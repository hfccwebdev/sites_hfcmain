<?php

/**
 * Module page callbacks.
 */
class EventLocationLookup {

  /**
   * Custom callback for /events/add.
   */
  public static function location($string = '') {
    $output = [];
    if ($string) {
      $result = db_query_range("
        SELECT TRIM(field_news_event_location_value) AS value FROM {field_data_field_news_event_location}
        WHERE LOWER(field_news_event_location_value) LIKE LOWER(:string)
        GROUP BY value ORDER BY value
      ", 0, 10, [':string' => "%{$string}%"])->fetchAll();

      foreach ($result as $item) {
        $output[$item->value] = check_plain($item->value);
      }
    }
    drupal_json_output($output);
  }
}
