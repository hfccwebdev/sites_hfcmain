<?php

/**
 * Provides the HFC News 1.1 API.
 *
 * All api/1.0 endpoints are provided by the
 * Services and Services Views contrib modules.
 */
class MysiteContentApi extends HfcGlobalBaseContentApi {

  /**
   * Returns the full news index.
   */
  public static function index(string $type): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;

    self::checkApiKey();
    $match = "type = '$type'";

    $result = db_query("
      SELECT nid, uuid, changed FROM {node}
      WHERE status = 1 AND $match
    ")->fetchAllAssoc('nid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * Returns the academic calendar index.
   */
  public static function academic(): void {

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;
    $result = db_query("
      SELECT nid, uuid, changed FROM {node} n
      JOIN {field_data_field_news_tags} t ON t.revision_id = n.vid
      WHERE status = 1
      AND type = 'news'
      AND field_news_tags_tid IN (113, 114)
    ")->fetchAllAssoc('nid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * Returns news content.
   */
  public static function content(object $node): void {

    self::checkApiKey();

    unset($node->data);

    // Convert all file URIs.
    foreach ([
      'field_agenda',
      'field_attachments',
      'field_board_reports',
      'field_gallery_photos',
      'field_header_image',
      'field_minutes',
      'field_news_attachments',
      'field_news_photo',
      'field_original_documents',
      'field_other_documents',
      'field_person_photo',
      'field_photo',
      'field_room_photos',
    ] as $fieldname) {
      if (!empty($node->{$fieldname})) {
        self::convertFileUri($node->{$fieldname});
      }
    }

    // Load child paragraph entities.
    if (!empty($node->field_paragraphs)) {
      self::loadParagraphs($node->field_paragraphs);
    }

    $node->url = '/' . drupal_get_path_alias("node/{$node->nid}");

    drupal_json_output($node);
    drupal_exit();
  }

  /**
   * Returns all news tags.
   */
  public static function tags(): void {
    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;
    $result = db_query("
      SELECT tid FROM {taxonomy_term_data} WHERE vid = 1
    ")->fetchCol();
    $terms = taxonomy_term_load_multiple($result);
    foreach (array_keys($terms) as $tid) {
      $terms[$tid]->parents = taxonomy_get_parents($tid);
    }
    drupal_json_output($terms);
  }

  /**
   * Return UUID list of unpublished news.
   */
  public static function newsUnpublished(): void {
    $GLOBALS['conf']['cache'] = 0;
    $result = db_query("
      SELECT nid, uuid, changed FROM {node}
      WHERE status = 0 AND type = 'news'
    ")->fetchAllAssoc('nid');
    drupal_json_output($result);
    drupal_exit();
  }

}
