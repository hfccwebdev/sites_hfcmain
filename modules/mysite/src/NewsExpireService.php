<?php

/**
 * Defines the News Expiration and Retirement Interface.
 */
interface NewsExpireServiceInterface {

  /**
   * Create an instance of this class.
   */
  public static function create();

  /**
   * Update node to set expiration flags.
   *
   * @param $node
   *   The node to update.
   * @param $op
   *   The operation to perform.
   *   - update: Update node values.
   *   - retire: Reset retire date and update values.
   * @param $values
   *   The values passed from the form.
   */
  public function doUpdate($node, $op, $values);

  /**
   * Checks for expired content.
   */
  public function checkExpiredContent();

}

/**
 * Defines the News Expiration and Retirement Service.
 */
class NewsExpireService implements NewsExpireServiceInterface {

  /**
   * {@inheritdoc}
   */
  public static function create() {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function doUpdate($node, $op, $values) {

    if ($op == 'retire') {
      $retire_date = strtotime("00:00", REQUEST_TIME) - 86400;
      drupal_set_message(t('Changed article retire date to %date', ['%date' => format_date($retire_date, 'custom', 'm/d/Y')]));
    }
    elseif (!empty($values['retire_date']) && strtotime($values['retire_date'])) {
      $retire_date = strtotime($values['retire_date']);
    }

    $node->field_news_retire_date[LANGUAGE_NONE][0]['value'] = $retire_date;

    $promote = [];
    foreach ($values['field_news_promote'] as $value) {
      if (!empty($value)) {
        $promote[] = ['value' => $value];
      }
    }

    $node->field_news_promote[LANGUAGE_NONE] = $promote;

    $node->field_evergreen[LANGUAGE_NONE][0]['value'] = !empty($values['flags']['evergreen']) ? 1 : 0;

    if (!empty($values['expire_date']) && strtotime($values['retire_date'])) {
      $expire_date = strtotime($values['expire_date']);
    }

    $node->field_expire_content[LANGUAGE_NONE][0]['value'] = $expire_date;

    $node->revision = 1;
    $node->log = t('Updated promotion and expiration info from retire tab.');
    node_save($node);
  }

  /**
   * {@inheritdoc}
   */
  public function checkExpiredContent() {

    if (
      db_table_exists('field_data_field_expire_content') &&
      db_field_exists('field_data_field_expire_content', 'field_expire_content_value')
    ) {

      $result = db_query("SELECT nid FROM {node} n
        LEFT JOIN {field_data_field_expire_content} x ON n.vid = x.revision_id AND (x.entity_type = 'node' AND x.deleted = '0')
        WHERE (n.status = 1) AND (x.field_expire_content_value IS NOT NULL ) AND (x.field_expire_content_value < :now) LIMIT 1
      ", [':now' => REQUEST_TIME])->fetchAll();

      if (!empty($result)) {
        drupal_set_message(t('Some published content has expired.
          <a href="/admin/content/maintenance/expired">Check the list for details</a>.
        '), 'warning');
      }
    }

    if (
      db_table_exists('field_data_field_review_after_date') &&
      db_field_exists('field_data_field_review_after_date', 'field_review_after_date_value')
    ) {

      $result = db_query("SELECT nid FROM {node} n
        LEFT JOIN {field_data_field_review_after_date} x ON n.vid = x.revision_id AND (x.entity_type = 'node' AND x.deleted = '0')
        WHERE (n.status = 1) AND (x.field_review_after_date_value IS NOT NULL ) AND (x.field_review_after_date_value < :now) LIMIT 1
      ", [':now' => REQUEST_TIME])->fetchAll();

      if (!empty($result)) {
        drupal_set_message(t('Some published content is flagged for review.
          <a href="/admin/content/maintenance">Check the list for details</a>.
        '), 'warning');
      }
    }
  }

}
