<?php

/**
 * Defines the Paragraphs Usage service.
 */
class MysiteParaUsageService {

  /**
   * Available details.
   *
   * @var array
   */
  private $bundles = [
    'block_reference' => 'getBlockReferenceDetails',
    'catalog_programs' => 'getCatalogProgramDetails',
    'contact_info' => 'getOfficeDetails',
    'multiple_office_contacts' => 'getMultipleOfficeDetails',
  ];

  /**
   * Stores Catalog office info.
   *
   * @var array
   */
  private static $offices = [];

  /**
   * Stores Catalog program info.
   *
   * @var array
   */
  private static $programs = [];

  /**
   * Create an instance of this class.
   */
  public static function create(): object {
    return new static();
  }

  /**
   * Get results from form selection.
   */
  public function results(array $values): array {
    $output = [];

    $query = db_select('paragraphs_item', 'p')
      ->fields('p', ['item_id'])
      ->condition('p.bundle', $values['bundle'])
      ->condition('p.archived', 0);

    $ids = $query->execute()->fetchCol();
    $paragraphs = paragraphs_item_load_multiple($ids);
    foreach ($paragraphs as $id => $paragraph) {

      if ($parent = $this->getParent($paragraph)) {
        $paragraphs[$id]->parent = $parent;
        // Try to get details about this paragraph.
        $scanner = $this->bundles[$paragraph->bundle] ?? 'invalid';
        $paragraphs[$id]->details = method_exists($this, $scanner)
          ? $this->$scanner($paragraph)
          : NULL;
      }
      else {
        unset($paragraphs[$id]);
      }
    }

    $rows = array_map(function ($paragraph) {
      return [
        'nid' => $paragraph->parent->nid,
        'type' => $paragraph->parent->type,
        'node' => l(
          $paragraph->parent->title,
          "node/{$paragraph->parent->nid}"
        ),
        'path' => url("node/{$paragraph->parent->nid}"),
        'status' => $paragraph->parent->status ? 'Y' : 'N',
        'details' => $paragraph->details,
      ];
    }, $paragraphs);

    $output['results'] = [
      '#theme' => 'table',
      '#header' => [
        t('ID'),
        t('Type'),
        t('Title'),
        t('Path'),
        t('Published'),
        t('Details'),
      ],
      '#rows' => $rows,
    ];

    return $output;
  }

  /**
   * Get the parent node. Recursively if necessary.
   */
  private function getParent(object $paragraph) {

    $table = "field_data_{$paragraph->field_name}";
    $revision = "{$paragraph->field_name}_revision_id";

    $field = db_select($table, 'f')
      ->fields('f')
      ->condition($revision, $paragraph->revision_id)
      ->execute()
      ->fetchObject();

    if (!$field) {
      return;
    }

    if ($field->entity_type == 'node') {
      return node_load($field->entity_id);
    }
    elseif ($field->entity_type == 'paragraphs_item') {
      $parent = paragraphs_item_load($field->entity_id);
      return $this->getParent($parent);
    }
  }

  /**
   * Details callback for Block Reference paragraphs.
   */
  private function getBlockReferenceDetails(object $paragraph): string {

    $block = $paragraph->field_para_block_reference[LANGUAGE_NONE][0]['moddelta'] ?? 'error';

    [$module, $delta] = explode(':', $block);

    // @see views_block_view()
    // If this is 32, this should be an md5 hash.
    if (strlen($delta) == 32) {
      $delta = variable_get('views_block_hashes', [])[$delta] ?? "error getting $delta";
    }

    return "$module:$delta";
  }

  /**
   * Details callback for Catalog Programs paragraphs.
   */
  private function getCatalogProgramDetails(object $paragraph): string {
    if (empty($paragraph->field_catalog_programs)) {
      return 'none';
    }

    $items = array_map(function ($item) {
      return $this->getProgram($item['value']);
    }, $paragraph->field_catalog_programs[LANGUAGE_NONE]);
    $list = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return render($list);
  }

  /**
   * Details callback for Office Info paragraphs.
   */
  private function getOfficeDetails(object $paragraph): string {
    if (empty($paragraph->field_office)) {
      return 'none';
    }
    return $this->getOffice($paragraph->field_office[LANGUAGE_NONE][0]['target_id']);
  }

  /**
   * Details callback for Multiple Office Contacts paragraphs.
   */
  private function getMultipleOfficeDetails(object $paragraph): string {
    if (empty($paragraph->field_office_multiple)) {
      return 'none';
    }

    $items = array_map(function ($item) {
      return $this->getOffice($item['target_id']);
    }, $paragraph->field_office_multiple[LANGUAGE_NONE]);
    $list = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return render($list);

  }

  /**
   * Gets a program link.
   */
  private function getProgram($nid): string {
    if (empty(static::$programs)) {
      static::$programs = WebServicesClient::getPrograms();
    }
    if ($program = static::$programs[$nid] ?? NULL) {
      return l($program['title'], $program['path']);
    }
    drupal_set_message(t('Cannot load program !id', ['!id' => $nid]), 'error');
    return $nid;
  }

  /**
   * Gets an office name.
   */
  private function getOffice($nid): string {
    if (empty(static::$offices)) {
      static::$offices = WebServicesClient::getOfficesOpts();
    }
    return static::$offices[$nid] ?? $nid;
  }

}
