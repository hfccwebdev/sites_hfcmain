<?php

/**
 * Defines tools used by the site.
 */
class MysiteTools {

  /**
   * Creates a new instance of this class.
   */
  public static function create() {
    return new static();
  }

  /**
   * Format office hours for current term.
   */
  public static function currentTermOfficeHours($office) {

    $output = [];

    $terms = [
      'WI' => ['field' => 'winter_hours', 'label' => 'Winter'],
      'SP' => ['field' => 'spring_hours', 'label' => 'Spring'],
      'SU' => ['field' => 'summer_hours', 'label' => 'Summer'],
      'FA' => ['field' => 'fall_hours', 'label' => 'Fall'],
    ];

    $term = drupal_substr(WebServicesClient::getCurrentTerm(), -2);

    if ($hours = $office[$terms[$term]['field']]) {

      if ($rows = self::formatOfficeSemesterHours($hours)) {
        $output[] = [
          '#field_name' => 'hfccwsclient_hours_today',
          '#label' => t('Hours during @label Semester', ['@label' => $terms[$term]['label']]),
          '#label_display' => 'above',
          '#items' => $rows,
          '#theme' => 'hfcc_global_pseudo_field',
        ];
      }
    }

    return $output;
  }

  /**
   * Format hours for a term.
   */
  private static function formatOfficeSemesterHours($hours) {

    $labels = [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'];

    $days = [];

    foreach ($hours as $segment) {
      if (!empty($segment['day']) && !empty($segment['starthours']) && !empty($segment['endhours'])) {
        $weekday = ($segment['day'] == 0) ? 7 : $segment['day'];
        $days[$weekday][] = WebServicesClient::formatTime($segment['starthours']) . ' - ' . WebServicesClient::formatTime($segment['endhours']);
      }
    }

    $rows = [];

    if (!empty($days)) {
      ksort($days);
      if (
        !empty($days[1]) && !empty($days[2]) && !empty($days[3]) && !empty($days[4]) && !empty($days[5]) &&
        ($days[1] == $days[2]) && ($days[1] == $days[3]) && ($days[1] == $days[4]) && ($days[1] == $days[5])
      ) {
        $labels[1] = 'Monday - Friday';
        for ($i = 2; $i < 6; $i++) {
          unset($days[$i]);
        }
      }
      elseif (
        !empty($days[1]) && !empty($days[2]) && !empty($days[3]) && !empty($days[4]) &&
        ($days[1] == $days[2]) && ($days[1] == $days[3]) && ($days[1] == $days[4])
      ) {
        $labels[1] = 'Monday - Thursday';
        for ($i = 2; $i < 5; $i++) {
          unset($days[$i]);
        }
      }
      elseif (
        !empty($days[1]) && !empty($days[2]) && !empty($days[3]) &&
        ($days[1] == $days[2]) && ($days[1] == $days[3])
      ) {
        $labels[1] = 'Monday - Wednesday';
        unset($days[2]);
        unset($days[3]);
      }
      elseif (
        !empty($days[2]) && !empty($days[3]) && !empty($days[4]) && !empty($days[5]) &&
        ($days[2] == $days[3]) && ($days[2] == $days[4]) && ($days[2] == $days[5])
      ) {
        $labels[2] = 'Tuesday - Friday';
        for ($i = 3; $i < 6; $i++) {
          unset($days[$i]);
        }
      }
      elseif (
        !empty($days[2]) && !empty($days[3]) && !empty($days[4]) &&
        ($days[2] == $days[3]) && ($days[2] == $days[4])
      ) {
        $labels[2] = 'Tuesday - Thursday';
        unset($days[3]);
        unset($days[4]);
      }

      foreach ($days as $weekday => $times) {
        $rows[$weekday] = [
          '#markup' => t(
            '@label: @times',
            ['@label' => $labels[$weekday], '@times' => implode(', ', $times)]
          ),
        ];
      }
      return $rows;
    }
  }

  /**
   * Build a person's full name from fields.
   *
   * @param $node
   *   The source node.
   *
   * @return string
   *   The person's full name.
   */
  public function personFullName($node) {

    $parts = [];

    if (!empty($node->field_person_firstname[LANGUAGE_NONE][0]['value'])) {
      $parts[] = $node->field_person_firstname[LANGUAGE_NONE][0]['value'];
    }
    if (!empty($node->field_person_lastname[LANGUAGE_NONE][0]['value'])) {
      $parts[] = $node->field_person_lastname[LANGUAGE_NONE][0]['value'];
    }

    $fullname = implode(' ', $parts);

    if (!empty($node->field_person_suffix[LANGUAGE_NONE][0]['value'])) {
      $fullname .= ', ' . $node->field_person_suffix[LANGUAGE_NONE][0]['value'];
    }

    return trim($fullname);
  }

  /**
   * Update employee HANK fields.
   *
   * @param $node
   *   The node to update.
   *
   * @return bool
   *   Returns TRUE if any data was changed.
   */
  public function updateEmployeeHankFields($node) {
    $output = FALSE;
    $id = !empty($node->field_person_hank_id[LANGUAGE_NONE]) ? $node->field_person_hank_id[LANGUAGE_NONE][0]['value'] : NULL;
    if ($id) {
      if ($staffdir = WebServicesClient::getStaffdir(['id=' . $id, 'all=true'])) {
        $staffdir = $this->firstChild($staffdir);

        foreach (['firstname', 'lastname', 'suffix', 'title', 'phone_number'] as $field) {

          $old = !empty($node->{"field_person_$field"}[LANGUAGE_NONE][0]['value'])
            ? trim($node->{"field_person_$field"}[LANGUAGE_NONE][0]['value'])
            : NULL;

          $new = !empty($staffdir[$field])
            ? trim($staffdir[$field])
            : NULL;

          if (!empty($new) && (strcmp($old, $new) !== 0)) {
            $node->{"field_person_$field"}[LANGUAGE_NONE][0]['value'] = $staffdir[$field];
            $output = TRUE;
          }
        }
      }
      else {
        drupal_set_message(t(
          'Could not load Employee Directory info for @name (@id)',
          ['@name' => $node->title, '@id' => $node->nid]
        ), 'warning');

        if (user_access('access site reports') && $hankdata = $this->getHankPerson($id)) {
          drupal_set_message(t(
            '@id @name has role @role and @flag. Term date @term',
            [
              '@id' => $hankdata->H19_IDV_ID,
              '@name' => $node->title,
              '@role' => !empty($hankdata->H19_IDV_ROLE) ? $hankdata->H19_IDV_ROLE : '(none)',
              '@flag' => !empty($hankdata->H19_IDV_STATUS_FLAG) ? $hankdata->H19_IDV_STATUS_FLAG : '(none)',
              '@term' => !empty($hankdata->H19_IDV_TERM_DATE) ? $hankdata->H19_IDV_TERM_DATE : 'is not set',
            ]
          ), 'warning');
        }

        $node->field_person_hank_id = NULL;
        $output = TRUE;
      }
    }
    $fullname = $this->personFullName($node);
    if (strcmp($node->title, $fullname) !== 0) {
      $node->title = $fullname;
      $output = TRUE;
    }

    return $output;
  }

  /**
   * Get HANK API person data.
   */
  public function getHankPerson($id) {
    $id = check_plain($id);
    if (is_numeric($id)) {
      $id = substr("0000000" . $id, -7, 7);
    }
    else {
      $id = drupal_strtolower($id);
    }

    if ($hank_identity = HfcHankApi::getData('hank-identity-info', $id, NULL, FALSE)) {
      return $this->firstChild($hank_identity);
    }
  }

  /**
   * Returns the first child element of an array.
   *
   * @see element_children()
   */
  public function firstChild($element) {
    if (is_array($element)) {
      $children = element_children($element);
      $first = reset($children);
      return !is_null($first) ? $element[$first] : NULL;
    }
  }

  /**
   * Format dates for upcoming events list.
   */
  public function formatEventDates($item) {

    if (!empty($item['value2']) && !$this->sameDay($item['value'], $item['value2'])) {

      if ($this->isFuture($item['value'])) {
        $start_date = $this->eventDateFormatter($item['value'], TRUE);
        $connector = 'to';
        $end_date = $this->eventDateFormatter($item['value2'], TRUE);
      }
      else {
        $start_date = t('<span class="today">Today</span>');
        $connector = '<br>to<br>';
        $end_date = format_date($item['value2'], 'custom', 'M j');
      }

      $start_wrap = '<span class="date-display-start">' . $start_date . '</span>';
      $end_wrap = '<span class="date-display-end">' . $end_date . '</span>';

      $output = "<span class=\"date-display-range\"> $start_wrap $connector $end_wrap</span>";
    }
    else {
      $output = '<span class="date-display-single">' . $this->eventDateFormatter($item['value']) . '</span>';
    }
    return $output;
  }

  /**
   * Format dates for upcoming events list.
   */
  public function formatNewsGridEventDates($event_date) {
    if (!empty($event_date)) {

      $formatter = function ($timestamp) {
        return $this->isFuture($timestamp) ? format_date($timestamp, 'custom', 'M j') : 'Today';
      };

      foreach ($event_date as $item) {

        $start_date = $item['value'];
        $end_date = $item['value2'] ?? $item['value'];

        // @todo Only test for sameDay on date_all_day events else use ending time.
        // We can finish after #7176 is resolved, because it will change data.
        if (!empty($start_date) && ($this->sameDay($end_date) || $this->isFuture($end_date))) {

          $formatted_start = $formatter($start_date);
          $formatted_end = $formatter($end_date);

          if ($formatted_start !== $formatted_end) {
            return "{$formatted_start} to {$formatted_end}";
          }
          else {
            return $formatted_start;
          }
        }
      }
    }
  }

  /**
   * Returns an array of year options.
   *
   * @param int $start
   *   Starting year.
   *
   * @return int[]
   *   An array of values.
   */
  public function getYearOpts($start = 1985) {
    $results = [];
    for ($i = $start; $i < date("Y") + 5; $i++) {
      $results[$i] = $i;
    }
    return $results;
  }

  /**
   * Get Taxonomy Term Content by Year.
   *
   * @param int[]|int $tids
   *   Taxonomy term ids.
   * @param int $year
   *   The year to filter.
   *
   * @return array
   *   A renderable array.
   *
   * @see taxonomy_select_nodes()
   */
  public function getTermContentByYear($tids, $year) {

    $tids = is_array($tids) ? $tids : [$tids];

    $query = db_select('taxonomy_index', 't');
    $query->addTag('node_access');
    $query->condition('tid', $tids, 'IN');
    $query->addField('t', 'nid');
    $query->join('node', 'n', 'n.nid = t.nid');
    $query->addField('n', 'title');
    $query->join('field_data_field_news_release_date', 'd', "d.entity_id = t.nid AND d.entity_type = 'node'");
    $query->addField('d', 'field_news_release_date_value', 'release_date');

    $query->leftJoin('field_data_field_news_event_short_title', 'short_title',
      "short_title.entity_id = t.nid AND short_title.entity_type = 'node'");
    $query->addField('short_title', 'field_news_event_short_title_value', 'short_title');
    $query->leftJoin('field_data_field_news_alt_link', 'alt_link',
      "alt_link.entity_id = t.nid AND alt_link.entity_type = 'node'");
    $query->addField('alt_link', 'field_news_alt_link_url', 'alt_link_url');

    if (!empty($year)) {
      $query->where("FROM_UNIXTIME(d.field_news_release_date_value, '%Y') = :year", [':year' => $year]);
    }

    $query->orderBy('d.field_news_release_date_value', 'desc');

    if ($result = $query->execute()->fetchAll()) {
      return array_map(function ($item) {
        return [
          '#markup' => t('<p><a href="!url">@title</a> - !date</p>', [
            '@title' => $item->short_title ?? $item->title,
            '!url' => url($item->alt_link_url ?? "node/{$item->nid}"),
            '!date' => format_date($item->release_date, 'custom', 'M j, Y'),
          ]),
        ];
      }, $result);
    }
  }

  /**
   * Apply custom formatting for event dates.
   */
  private function eventDateFormatter($timestamp, $date_only = FALSE) {
    $date_time_format = 'M\<\b\r\>\<\s\p\a\n \c\l\a\s\s\=\"\d\a\y\"\>j\<\/\s\p\a\n\>\<\b\r\>g:ia';
    $date_only_format = 'M\<\b\r\>\<\s\p\a\n \c\l\a\s\s\=\"\d\a\y\"\>j\<\/\s\p\a\n\>';

    $format = ($date_only || format_date($timestamp, 'custom', 'Hi') == '0000')
      ? $date_only_format
      : $date_time_format;

    return format_date($timestamp, 'custom', $format);
  }

  /**
   * Compare dates by day only.
   *
   * @param int $start_date
   *   Starting timestamp.
   * @param int $end_date
   *   Ending timestamp.
   *
   * @return bool
   *   Returns TRUE if both datestamps refer to the same day.
   */
  public function sameDay($start_date, $end_date = REQUEST_TIME) {
    return format_date($start_date, 'custom', 'Ymd') === format_date($end_date, 'custom', 'Ymd');
  }

  /**
   * Determine if indicated date is in the future.
   *
   * @param int $timestamp
   *   Timestamp to check.
   *
   * @return bool
   *   Returns TRUE if the datestamp is in the future.
   */
  public function isFuture($timestamp) {
    return ($timestamp > REQUEST_TIME) && !$this->sameDay($timestamp);
  }

  /**
   * Determine prior date value by day only.
   *
   * @param int $start_date
   *   Starting timestamp.
   * @param int $end_date
   *   Ending timestamp or current request time.
   *
   * @return bool
   *   Returns TRUE if start_date is prior to end_date.
   */
  public function isBefore(int $start_date, int $end_date = REQUEST_TIME): bool {
    return format_date($start_date, 'custom', 'Ymd') < format_date($end_date, 'custom', 'Ymd');
  }

  /**
   * Quick content inventory.
   */
  public function inventory() {
    $query = db_select('node', 'n');
    $query->fields('n', ['nid', 'title', 'type']);
    $query->leftJoin("url_alias", "u", "u.source = CONCAT('node/', n.nid)");

    $query->condition('n.status', NODE_PUBLISHED);
    $query->condition('n.type', ['marketing_landing_page', 'page'], 'IN');
    $query->addField('u', 'alias');
    $query->orderBy('u.alias');
    $query->orderBy('n.title');
    $result = $query->execute()->fetchAll();

    echo json_encode($result);
  }

}
