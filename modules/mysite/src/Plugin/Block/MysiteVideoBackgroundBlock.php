<?php

/**
 * Defines the Stupid Video Background Block.
 */
class MysiteVideoBackgroundBlock extends HfcGlobalBaseBlock {
  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Video Background Block'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $output['hello'] = [
      '#markup' => t('<h1>Hello there</h1>'),
    ];
  }
}
