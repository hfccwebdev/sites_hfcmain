<?php

/**
 * Defines the News Archive Message Block.
 */
class NewsArchiveMessageBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Old News Archive Message'),
      'cache' => DRUPAL_NO_CACHE, // DRUPAL_CACHE_PER_PAGE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    if ($this->isOldNews()) {
      $output[] = ['#markup' => t('
        <strong>Notice</strong>: This article is more than one year old and is part of the Henry Ford College news archive.
        Information in the article may be outdated. For the most current news and information about Henry Ford College,
        please visit <a href="https://www.hfcc.edu/news">hfcc.edu/news</a>,
        or contact <a href="mailto:communications@hfcc.edu">communications@hfcc.edu</a>.
      ')];
    }
  }

  /**
   * Determine if this page contains "old" news.
   *
   * @return bool
   *   Returns TRUE if news is old.
   */
  private function isOldNews() {
    if ($node = menu_get_object('node')) {

      $old = REQUEST_TIME - (86400 * 366);

      // Check evergreen flag.
      if (!empty($node->field_evergreen)) {
        if ($node->field_evergreen[LANGUAGE_NONE][0]['value']) {
          // If it's marked evergreen, it's _never_ old!
          return FALSE;
        }
      }

      // Check Event Date.
      if (!empty($node->field_news_event_date)) {
        // Check the end date, then the start date if necessary.
        if (!empty($node->field_news_event_date[LANGUAGE_NONE][0]['value2'])) {
          if ($node->field_news_event_date[LANGUAGE_NONE][0]['value2'] < $old) {
            return TRUE;
          }
        }
        elseif (!empty($node->field_news_event_date[LANGUAGE_NONE][0]['value'])) {
          if ($node->field_news_event_date[LANGUAGE_NONE][0]['value'] < $old) {
            return TRUE;
          }
        }
      }

      // Do NOT check Retire Date.
      // Per meeting on 9/3/19, Retire Date is for promotion only,
      // and should not be considered for this purpose.
      // @see https://dvc.hfcc.net/webadmin/issues/issue7422

      // Check Release Date.
      elseif (!empty($node->field_news_release_date)) {
        if ($node->field_news_release_date[LANGUAGE_NONE][0]['value'] < $old) {
          return TRUE;
        }
      }

      // Check creation date.
      elseif ($node->created < $old) {
        return TRUE;
      }
    }
  }

}
