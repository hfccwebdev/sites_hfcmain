<?php

/**
 * Defines the Front Page Image Review Block.
 */
class FrontPageImageReviewBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Front Page Image Review'),
      'cache' => DRUPAL_NO_CACHE, // DRUPAL_CACHE_PER_PAGE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    if (
      ($node = menu_get_object('node')) &&
      node_access('view', $node) &&
      !empty($node->field_photo[LANGUAGE_NONE][0]['fid'])
    ) {
      $path = file_create_url($node->field_photo[LANGUAGE_NONE][0]['uri']);
      $inline_style = "background-image: url($path)";
      $alt_link = !empty($node->field_news_alt_link)
        ? file_create_url($node->field_news_alt_link[LANGUAGE_NONE][0]['url'])
        : '#';
      $cut_line = !empty($node->field_news_cut_line)
        ? $node->field_news_cut_line[LANGUAGE_NONE][0]['safe_value']
        : "";

      $output[] = [
        '#markup' => t("
          <div class=\"slides-master-header-container\" style=\"min-height: 500px;\">
            <div class=\"slide active\">
              <div id=\"header-@nid\" class=\"header-image\" style=\"!image\">
                <a href=\"@alt_link\" class=\"no-link\">
                  <div class=\"billboard-caption\">
                    <h2>@title</h2>!cut_line
                  </div>
                  </a>
              </div>
            </div>
          </div>", [
            '@nid' => $node->nid,
            '!image' => $inline_style,
            '@alt_link' => $alt_link,
            '@title' => $node->title,
            '!cut_line' => $cut_line,
          ]),
      ];
    }
  }

}
