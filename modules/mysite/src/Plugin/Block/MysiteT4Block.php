<?php

/**
 * Defines the Top Four Events Block.
 */
class MysiteT4Block extends HfcGlobalBaseBlock {

  /**
   * Stores the site tools service.
   */
  private MysiteTools $tools;

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->tools = MysiteTools::create();
  }

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Top Four Events Advanced'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    if ($events = $this->getEvents()) {
      $output['header'] = [
        '#markup' => t('<h2 class="sm-title">Upcoming Events</h2>'),
      ];
      $output['events'] = [
        '#prefix' => '<div class="upcoming-events-list"><div class="view-content">',
        '#suffix' => '</div></div>',
      ];
      foreach ($events as $item) {
        $row = [
          '#prefix' => '<div class="event-card">',
          '#suffix' => '</div>',
        ];

        $datevalue = [
          'value' => $item->start_date,
          'value2' => $item->end_date,
        ];
        $row['event_date'] = [
          '#prefix' => '<div class="event-date">',
          '#markup' => $this->tools->formatEventDates($datevalue),
          '#suffix' => '</span></div>',
        ];

        $title = $item->short_title ?? $item->title;
        $link = $item->alt_link ?? "node/{$item->nid}";
        $row['title'] = [
          '#prefix' => '<div class="event-info">',
          '#markup' => l($title, $link),
          '#suffix' => '</span></div>',
        ];

        $output['events'][] = $row;
      }
      $output['footer'] = [
        '#markup' => t('<p><a href="/events" class="hfc-button">More Events</a></p>'),
      ];
    }
  }

  /**
   * Load the relevant news items.
   *
   * Note: This method uses lots of hard-coded node IDs.
   * That's a quick fix for now. We will do this the right way
   * when we rewrite this for Drupal 9/10.
   *   - 5708: Homepage.
   *   - 6167: Student news.
   */
  private function getEvents(): array {
    $node = menu_get_object('node');

    if ($node && in_array($node->nid, [5708, 5521, 6167, 5444])) {

      $query = db_select('field_data_field_news_event_date', 'event_date');
      $query->join('node', 'n', "n.vid = event_date.revision_id");
      $query->join('field_data_field_news_promote', 'promote', "promote.revision_id = event_date.revision_id");
      $query->leftjoin('field_data_field_important_event', 'important', "important.revision_id = event_date.revision_id");
      $query->leftjoin('field_data_field_news_event_short_title', 'short_title', "short_title.revision_id = event_date.revision_id");
      $query->leftjoin('field_data_field_news_alt_link', 'alt_link', "alt_link.revision_id = event_date.revision_id");

      $query->fields('n', ['nid', 'vid', 'title']);
      $query->addField('event_date', 'field_news_event_date_value', 'start_date');
      $query->addField('event_date', 'field_news_event_date_value2', 'end_date');
      // $query->addField('promote', 'field_news_promote_value', 'promoted');
      $query->addField('important', 'field_important_event_value', 'important');
      $query->addField('short_title', 'field_news_event_short_title_value', 'short_title');
      $query->addField('alt_link', 'field_news_alt_link_url', 'alt_link');

      $query->condition('n.status', NODE_PUBLISHED);

      // @todo: Make this configurable in Drupal 10 version.
      if (in_array($node->nid, [5708, 5521])) {
        $query->condition('promote.field_news_promote_value', 'evtfront');
      }
      elseif ($node->nid == 6167) {
        $query->condition('promote.field_news_promote_value', 'evtstudent');
      }
      elseif ($node->nid == 5444) {
        $query->condition('promote.field_news_promote_value', 'evtemployee');
      }

      $query->condition('event_date.field_news_event_date_value2', REQUEST_TIME, '>=');

      $query->groupBy('event_date.field_news_event_date_value');
      $query->groupBy('n.nid');

      $query->orderBy('event_date.field_news_event_date_value', 'ASC');
      $query->orderBy('n.nid', 'ASC');

      $result = $query->execute()->fetchAll();

      // Filter matches. Only one occurrance per node.
      $output = [];
      $matches = [];
      foreach ($result as $item) {
        if (!isset($matches[$item->nid])) {
          $matches[$item->nid] = TRUE;
          $output[] = $item;
        }
      }
      // Limit output to first four matches.
      return array_slice($output, 0, 4);
    }
    return [];
  }

}
