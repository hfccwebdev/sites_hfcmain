<?php

/**
 * Defines the Top Four Events Block.
 */
class MysiteEventsGridBlock extends HfcGlobalBaseBlock {

  /**
   * Stores the site tools service.
   */
  private MysiteTools $tools;

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->tools = MysiteTools::create();
  }

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Upcoming Events Grid Advanced'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    if ($events = $this->getEvents()) {
      $output['header'] = [
        '#markup' => t('<h2 class="block-title">Upcoming Events</h2>'),
      ];
      $output['events'] = [
        '#prefix' => '<div class="upcoming-events-list view-flex cols-4"><div class="view-content">',
        '#suffix' => '</div></div>',
      ];
      foreach ($events as $item) {
        $row = [
          '#prefix' => '<div class="views-row event-card">',
          '#suffix' => '</div>',
        ];

        $node = node_load($item->nid);
        // Must clone the object here for date mangling.
        $event = clone($node);
        $this->checkPastEvents($event, $item->start_date);
        $row["node"] = node_view($event, 'newsgrid');
        $output['events'][] = $row;
      }
      $output['footer'] = [
        '#markup' => t(
          '<p><a href="/events/@tid/upcoming" class="hfc-button">More Events</a></p>',
          ['@tid' => $item->tid]
        ),
      ];
    }
  }

  /**
   * Load the relevant news items.
   *
   * Note: This method uses lots of hard-coded node IDs.
   * That's a quick fix for now. We will do this the right way
   * when we rewrite this for Drupal 9/10.
   *   - 5708: Homepage.
   *   - 6167: Student news.
   */
  private function getEvents(): array {
    $node = menu_get_object('node');

    $query = db_select('field_data_field_news_event_date', 'event_date');
    $query->join('node', 'n', "n.vid = event_date.revision_id");
    $query->leftjoin('field_data_field_important_event', 'important', "important.revision_id = event_date.revision_id");
    $query->leftjoin('field_data_field_news_event_short_title', 'short_title', "short_title.revision_id = event_date.revision_id");
    $query->leftjoin('field_data_field_news_alt_link', 'alt_link', "alt_link.revision_id = event_date.revision_id");
    $query->join('field_data_field_news_tags', 'news_tags', "news_tags.revision_id = event_date.revision_id");
    $query->join('taxonomy_term_data', 'term_data', "term_data.tid = news_tags.field_news_tags_tid");
    $query->join('field_data_field_related_node', 'related_node', "related_node.entity_id = term_data.tid");

    $query->fields('n', ['nid', 'vid', 'title']);
    $query->addField('event_date', 'field_news_event_date_value', 'start_date');
    $query->addField('event_date', 'field_news_event_date_value2', 'end_date');
    $query->addField('important', 'field_important_event_value', 'important');
    $query->addField('short_title', 'field_news_event_short_title_value', 'short_title');
    $query->addField('alt_link', 'field_news_alt_link_url', 'alt_link');
    $query->addField('term_data', 'tid');

    $query->condition('n.status', NODE_PUBLISHED);
    $query->condition('related_node.field_related_node_target_id', $node->nid);
    $query->condition('event_date.field_news_event_date_value2', REQUEST_TIME, '>=');

    $query->groupBy('event_date.field_news_event_date_value');
    $query->groupBy('n.nid');

    $query->orderBy('event_date.field_news_event_date_value', 'ASC');
    $query->orderBy('n.nid', 'ASC');

    $query->range(0, 20);
    $result = $query->execute()->fetchAll();

    // Filter matches. Only one occurrance per node.
    $output = [];
    $matches = [];
    foreach ($result as $item) {
      if (!isset($matches[$item->nid])) {
        $matches[$item->nid] = TRUE;
        $output[] = $item;
      }
    }

    // If resulting block output is fewer than four items,
    // try again without filtering.
    if (count($output) < 4) {
      $output = [];
      $matches = [];
      foreach ($result as $item) {
        $output[] = $item;
      }
    }

    // Limit output to first four matches.
    return array_slice($output, 0, 4);
  }

  /**
   * Remove past events from node.
   */
  private function checkPastEvents(object $node, int $display_date): void {
    foreach ($node->field_news_event_date[LANGUAGE_NONE] as $delta => $event) {
      if ($this->tools->isBefore($event['value'], $display_date)) {
        unset($node->field_news_event_date[LANGUAGE_NONE][$delta]);
      }
    }
  }

}
