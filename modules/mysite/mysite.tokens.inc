<?php

/**
 * @file
 * Token callbacks for this module.
 */

/**
 * Implements hook_token_info().
 */
function mysite_token_info() {

  $info = [];

  $info['tokens']['node']['person_full_name'] = [
    'name' => t('Person Full Name'),
    'description' => t('Full name of person from fields.'),
  ];

  $info['tokens']['node']['field_related_node_path'] = [
    'name' => t('Related Node Path'),
    'description' => t('Path of related node.'),
  ];

  $info['tokens']['term']['term_related_node_id'] = [
    'name' => t('Related Node ID'),
    'description' => t('Node ID of related node.'),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function mysite_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];
  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'person_full_name':
          $replacements[$original] = MysiteTools::create()->personFullName($data['node']);
          break;
        case 'field_related_node_path':
          if (!empty($data['node']->field_related_node)) {
            $path = 'node/' . reset($data['node']->field_related_node[LANGUAGE_NONE])['target_id'];
            $replacements[$original] = url($path, ['absolute' => TRUE]);
          }
          break;
      }
    }
  }
  elseif ($type == 'term') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'term_related_node_id':
          if (!empty($data['term']->field_related_node)) {
            $replacements[$original] = $data['term']->field_related_node[LANGUAGE_NONE][0]['target_id'];
          }
          break;
      }
    }
  }
  return $replacements;
}
