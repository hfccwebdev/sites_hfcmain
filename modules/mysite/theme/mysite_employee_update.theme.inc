<?php

/**
 * @file
 * Includes theme functions for mysite_employee_update_form elements.
 *
 * @see mysite_employee_update_form()
 */

/**
 * Theme the employee update form as a table.
 */
function theme_mysite_employee_update($variables) {
  $element = $variables['element'];

  $header = [
    'action' => t('Update'),
    'node_preview' => t('Current values'),
    'clone_preview' => t('New values'),
    'operations' => t('Operations'),
  ];

  $rows = [];
  foreach (element_children($element) as $key) {
    $row = [];
    $row['data'] = [];
    foreach ($header as $fieldname => $title) {
      $row['data'][] = drupal_render($element[$key][$fieldname]);
    }
    $rows[] = $row;
  }

  return theme('table', [
    'header' => $header,
    'rows' => $rows,
  ]);
}
