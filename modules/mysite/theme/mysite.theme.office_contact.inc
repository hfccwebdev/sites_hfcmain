<?php

/**
 * @file
 * Includes theme functions for office_contact.
 *
 * @see hfccwsclient.module
 * @see mysite.module
 */

/**
 * Theme preprocess function for theme_mysite_office_contact().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 * @see template_preprocess_hfc_office_hours()
 */
function template_preprocess_mysite_office_contact(&$variables, $hook) {
  $item = $variables['element'];

  $content = array(
    '#prefix' => '<div class="officeinfo">',
    '#suffix' => '</div>',
  );

  $content[] = MysiteTools::currentTermOfficeHours($item);

  if (!empty($item['office_location'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_location',
      '#label' => t('Location'),
      '#label_display' => 'hidden',
      '#markup' => t('Located in <a href="https://www.hfcc.edu/map">@office_location</a>', array("@office_location" => $item['office_location'])),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }
  else if (!empty($item['building'])) {
    $building = array('building' => $item['building'], '#theme' => 'hank_building', '#valcodes' => TRUE);
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_location',
      '#label' => t('Location'),
      '#label_display' => 'hidden',
      '#markup' => t('Located in the <a href="https://www.hfcc.edu/map">@building</a>', array("@building" => render($building))),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['phone_number'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_phone',
      '#label' => t('Phone Number'),
      '#label_display' => 'hidden',
      '#markup' => $item['phone_number'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['email'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_email',
      '#label' => t('Email Address'),
      '#label_display' => 'hidden',
      '#markup' => l($item['email'], 'mailto:' . $item['email']),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['note'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_note',
      '#label' => t('Note'),
      '#label_display' => 'hidden',
      '#markup' => check_markup($item['note'], filter_default_format()),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  $variables['content'] = $content;
}

/**
 * Theme function for mysite_office_contact.
 */
function theme_mysite_office_contact(&$variables) {
  return !empty($variables['content']) ? render($variables['content']) : NULL;
}
