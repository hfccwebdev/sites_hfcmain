<?php

/**
 * @file
 * Includes the Academic Calendar entry form.
 */

/**
 * Calendar item form.
 */
function mysite_accal_form($form, &$form_state) {

  $form['title'] = [
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 75,
    '#required' => TRUE,
  ];

  $form['event_settings']= [
    '#type' => 'fieldset',
    '#title' => t('Event Settings'),
    '#required' => TRUE,
  ];

  $form['event_settings']['start_date'] = [
    '#type' => 'date_popup',
    '#title' => t('Start Date/Time'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+2',
    '#date_timezone' => date_default_timezone(),
    '#required' => TRUE,
  ];

  $form['event_settings']['end_date'] = [
    '#type' => 'date_popup',
    '#title' => t('End Date/Time'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+2',
    '#date_timezone' => date_default_timezone(),
    '#required' => FALSE,
  ];

  $form['event_settings']['term'] = [
    '#type' => 'select',
    '#title' => t('Academic Term'),
    '#options' => WebServicesClient::getTermsOpts(["start_date" => REQUEST_TIME - 60 * 86400]),
    '#default_value' => variable_get('mysite_accal_default_term', NULL),
    '#description' => t('Change the default on <a href="/admin/config/hfc/mysite">site settings form</a>.'),
    '#required' => TRUE,
  ];

  $form['event_settings']['important'] = [
    '#type' => 'checkbox',
    '#title' => t('Important Event'),
  ];

  $form['body'] = [
    '#type' => 'textarea',
    '#title' => t('Event Description'),
    '#required' => FALSE,
  ];

  $form['taxonomy'] = [
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Tags'),
    '#tree' => TRUE,
  ];

  $query = db_select("taxonomy_term_data", "t");
  $query->fields("t", ["tid", "name"]);
  $query->join("taxonomy_term_hierarchy", "h", "h.tid = t.tid");
  $query->condition("t.vid", 1);
  $query->condition("h.parent", 0);
  $query->orderBy("t.weight");
  $parents = $query->execute()->fetchAllKeyed();

  $query = db_select("taxonomy_term_data", "t");
  $query->fields("t", ["tid", "name"]);
  $query->join("taxonomy_term_hierarchy", "h", "h.tid = t.tid");
  $query->fields("h", ["parent"]);
  $query->condition("t.vid", 1);
  $query->condition("h.parent", [198, 202], "IN");
  $query->orderBy("t.weight");
  $terms = $query->execute()->fetchAll();

  $options = [];
  foreach($terms as $term) {
    $options[$term->parent][$term->tid] = $term->name;
  }
  foreach($parents as $parent => $name) {
    if (!empty($options[$parent])) {
      $form["taxonomy"]["terms{$parent}"] = [
        '#type' => 'checkboxes',
        '#title' => t($name),
        '#options' => $options[$parent],
      ];
    }
  }

   $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}
/**
 * Calendar item form validation.
 */
function mysite_accal_form_validate($form, &$form_state) {

  $values = &$form_state['values'];

  if (!empty($values['end_date']) && (strtotime($values['start_date']) > strtotime($values['end_date']))) {
    form_set_error('end_date', t('Your event start date must come before your end date.'));
  }

  if (!empty($values['offsite_link'])) {
    if (!link_validate_url(trim($values['offsite_link']))) {
      form_set_error('offsite_link', t('The value provided for Alternate Link is not a valid URL.'));
    }
    else {
      $values['offsite_link'] = trim($values['offsite_link']);
    }
  }
}

/**
 * Calendar item form submission.
 */
function mysite_accal_form_submit($form, &$form_state) {

  $values = $form_state['values'];

  $node = new StdClass();
  $node->type = 'news';
  $node->status = NODE_PUBLISHED;
  $node->promote = NODE_NOT_PROMOTED;
  $node->language = LANGUAGE_NONE;

  node_object_prepare($node);

  $node->title = $values['title'];
  $node->field_news_event_date[LANGUAGE_NONE][0] = [
    'value' => strtotime($values['start_date']),
    'value2' => !empty($values['end_date']) ? strtotime($values['end_date']) : strtotime($values['start_date']),
    'rrule' => NULL,
    'timezone' => drupal_get_user_timezone(),
    'timezone_db' => date_default_timezone(),
    'date_type' => 'datestamp',
  ];

  $node->field_news_academic_term[LANGUAGE_NONE][0]['value'] = $values['term'];

  if (!empty($values['important'])) {
    $node->field_important_event[LANGUAGE_NONE][0]['value'] = $values['important'];
  }

  if (!empty($values['body'])) {
    $node->body[LANGUAGE_NONE][0] = ['value' => $values['body'], 'format' => 'markdown'];
  }

  $terms = [];
  foreach($values['taxonomy'] as $field) {
    foreach($field as $tid) {
      if($tid > 0) {
        $terms[] = ['tid' => $tid];
      }
    }
  }

  if(!empty($terms)) {
    $node->field_news_tags[LANGUAGE_NONE] = $terms;
  }

  node_save($node);

  if ($node->nid) {
    drupal_goto("node/{$node->nid}");
  }
}
