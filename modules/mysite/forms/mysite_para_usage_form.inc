<?php

/**
 * @file
 * Contains the Paragraphs usage form.
 */

/**
 * Paragraphs usage form.
 */
function mysite_para_usage_form($form, &$form_state): array {

  $bundles = paragraphs_bundle_load();
  $bundle_types = array_map(function ($bundle) {
    return $bundle->label;
  }, $bundles);

  $current_type = $form_state['values']['bundle'] ?? NULL;

  $form['page_title'] = [
    '#markup' => t('<h2>Paragraphs usage</h2>'),
  ];

  $form['bundle'] = [
    '#type' => 'select',
    '#title' => t('Bundle type'),
    '#options' => $bundle_types,
    '#default_value' => $current_type,
    '#required' => TRUE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'View usage',
  ];

  if (!empty($form_state['values']['results'])) {
    $form['results_title'] = [
      '#markup' => t(
        '<h3>Usage for @t type</h3>',
        ['@t' => $bundle_types[$current_type]]
      ),
    ];
    $form['results'] = $form_state['values']['results'];
  }

  return $form;
}

/**
 * Paragraphs usage form submit handler.
 */
function mysite_para_usage_form_submit($form, &$form_state): void {

  $values = $form_state['values'];

  $form_state['values']['results'] = MysiteParaUsageService::create()->results($values);

  $form_state['rebuild'] = TRUE;
}
