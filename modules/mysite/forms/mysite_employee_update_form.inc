<?php

/**
 * Employee update form.
 */
function mysite_employee_update_form($form, &$form_state) {

  if (!empty($form_state['values']['grid_items'])) {
    $changes = $form_state['values']['grid_items'];
  }
  else {
    $changes = SiteDataIntegrityTools::create()->scanEmployeeInfo();
  }

  $form['grid_items'] = [
    '#theme' => 'mysite_employee_update',
    '#tree' => TRUE,
  ];

  foreach ($changes as $change) {
    $key = $change['node']->nid;
    $form['grid_items'][$key] = [
      'node' => ['#type' => 'value', '#value' => $change['node']],
      'clone' => ['#type' => 'value', '#value' => $change['clone']],
      'action' => [
        '#type' => 'radios',
        '#options' => [0 => t('No change'), 1 => t('Without title'), 2 => t('All data')],
        '#default_value' => !empty($change['action']) ? $change['action'] : 0,
      ],
      'node_preview' => _mysite_employee_update_preview($change['node']),
      'clone_preview' => _mysite_employee_update_preview($change['clone']),
      'operations' => [
        '#markup' => implode('<br>', [
          l(t('view'), "node/{$change['node']->nid}"),
          l(t('edit'), "node/{$change['node']->nid}/edit", ['query' => drupal_get_destination()]),
          l(t('revisions'), "node/{$change['node']->nid}/revisions"),
        ]),
      ],
    ];
  }

  $form['update'] = [
    '#type' => 'submit',
    '#value' => t('Update selected info'),
  ];

  return $form;
}

/**
 * Submit handler for employee update form.
 */
function mysite_employee_update_form_submit($form, &$form_state) {

  $changes = array_filter($form_state['values']['grid_items'], function($change) {return $change['action'];});
  array_map('_mysite_employee_update_process', $changes);
}

/**
 * Builds render preview for mysite_employee_update_form().
 *
 * @param $node
 *   The node to display
 *
 * @return array
 *   A renderable array
 */
function _mysite_employee_update_preview($node) {

  if (!is_object($node)) {
    return ['#markup' => t('Error!')];
  }

  $output = [
    'title' => [
      '#prefix' => '<div><strong>',
      '#markup' => check_plain($node->title),
      '#suffix' => '</strong></div>',
    ],
  ];

  foreach (['hank_id', 'firstname', 'lastname', 'suffix', 'title', 'phone_number'] as $name) {
    $field = "field_person_$name";
    $output[$field] = [
      '#prefix' => '<div>',
      '#markup' => !empty($node->$field) ? check_plain($node->$field[LANGUAGE_NONE][0]['value']) : '<em>(null)</em>',
      '#suffix' => '</div>',
    ];
  }

  return $output;
}

/**
 * Process changes designated above.
 *
 * @param mixed $change
 *   An array containing the following elements
 *   - action: integer
 *       - 1 = update without position title
 *       - 2 = update all fields
 *   - node: the original node
 *   - clone: the cloned node with updated fields.
 */
function _mysite_employee_update_process($change) {

  switch ($change['action']) {
    case 1:
      $change['clone']->field_person_title = $change['node']->field_person_title;
    case 2:
      $change['clone']->revision = 1;
      $change['clone']->log = 'Employee Directory fields refreshed by employee update form.';
      node_save($change['clone']);
      watchdog(
        'employee_update',
        'Employee Directory info updated for %t (%n).',
        ['%t' => $change['clone']->title, '%n' => $change['clone']->nid],
        WATCHDOG_WARNING
      );
      return;
    default:
      drupal_set_message(t('Invalid action %a detected for %n!', ['%a'=> $change['action'], '%n' => $change['node']->nid]), 'error');
      break;
  }
}
