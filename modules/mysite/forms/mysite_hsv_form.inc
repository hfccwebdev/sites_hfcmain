<?php

/**
 * @file
 * Contains the editing form for the mysite_hsv_location entity type.
 */

/**
 * HSV location edit form.
 */
function mysite_hsv_form($form, &$form_state, $entity) {

  // Set the page title.
  if (empty($entity->is_new)) {
    $form_title = t('Editing @title', array('@title' => $entity->name));
  }
  else {
    $form_title = t('Add new location');
  }
  drupal_set_title($form_title);

  $form = array();

  // Include the entity as a value. This will not be passed to the browser,
  // but can be used by the submit handler.
  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Location Name'),
    '#default_value' => trim($entity->name),
    '#description' => t('The location name.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#default_value' => trim($entity->address),
    '#description' => t('The location address.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => FALSE,
  );

  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => trim($entity->city),
    '#description' => t('The location city.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => FALSE,
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Process HSV Location form submissions.
 *
 * @see mysite_hsv_form()
 */
function mysite_hsv_form_submit($form, &$form_state) {

  $entity = $form_state['values']['entity'];
  $entity->name = $form_state['values']['name'];
  $entity->address = $form_state['values']['address'];
  $entity->city = $form_state['values']['city'];
  entity_save('mysite_hsv_location', $entity);
  $form_state['redirect'] = 'admin/content/hsv-locations';
}
