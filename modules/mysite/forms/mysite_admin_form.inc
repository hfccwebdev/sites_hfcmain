<?php

/**
 * @file
 * Contains the module settings form.
 */

/**
 * Site-specific settings form.
 */
function mysite_admin_form() {
  $form = array();
  $form[] = array(
    '#type' => 'item',
    '#markup' => t('This form contains settings for custom features on this website.'),
  );
  $form['tuition_compare'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tuition Compare'),
    '#description' => t('This section controls settings for the HFC Tuition Compare pages.'),
  );
  $form['tuition_compare']['mysite_hfcc_tuition'] = array(
    '#type' => 'textfield',
    '#title' => t('HFC Tuition Amount'),
    '#size' => 10,
    '#default_value' => variable_get('mysite_hfcc_tuition', NULL),
    '#description' => t('Enter the HFC tuition cost for use with Tuition Compare pages.'),
  );
  $form['tuition_compare']['mysite_tuition_compare_notes'] = array(
    '#type' => 'textarea',
    '#title' => t('Tuition Compare Notes'),
    '#rows' => 5,
    '#default_value' => variable_get('mysite_tuition_compare_notes', NULL),
    '#description' => t('Enter notes to display on Tuition Compare pages. Enter one note per line, do not leave a blank line at the end.'),
  );
  $form['academic_calendar'] = [
    '#type' => 'fieldset',
    '#title' => t('Academic Calendar'),
    '#description' => t('This section controls settings for the Academic Calendar item creation form.'),
  ];
  $form['academic_calendar']['mysite_accal_default_term'] = [
    '#type' => 'select',
    '#title' => t('Academic Term'),
    '#options' => WebServicesClient::getTermsOpts(["start_date" => REQUEST_TIME - 60 * 86400]),
    '#default_value' => variable_get('mysite_accal_default_term', NULL),
    '#required' => TRUE,
  ];
  $form['other'] = [
    '#type' => 'fieldset',
    '#title' => t('Other settings'),
  ];
  $form['other']['mysite_display_header_placeholder'] = [
    '#type' => 'checkbox',
    '#title' => t('Display placeholder header image on published pages.'),
    '#default_value' => variable_get('mysite_display_header_placeholder', NULL),
  ];

  return system_settings_form($form);
}
