<?php

/**
 * @file
 * Contains the event submission form.
 */

/**
 * Event submission form.
 */
function mysite_event_form($form, $form_state) {
  /**
   * NOTE: TITLE CHARACTERS LIMITED TO 60 CHARACTERS:
   * OPTIMAL TITLE LENGTH - Google typically displays the first 50–60 characters of a title tag.
   * If you keep your titles under 60 characters, our research suggests that you can expect
   * about 90% of your titles to display properly. - MOZ
   */
  $form['html']['element'] = [
    '#type' => 'markup',
    '#markup' => t('<p>If your event occurs more than one day or time contact <a href="mailto:communications@hfcc.edu">communications@hfcc.edu</a> , do not use this form.</p>'),
  ];

  $form['title'] = [
    '#type' => 'textfield',
    '#title' => t('Event Title'),
    '#size' => 75,
    '#maxlength' => 75,
    '#required' => TRUE,
    '#description' => t('Please note that titles should be no more than 45 characters, to improve news displays and search engine friendliness. The limit for HFC is 75 characters, including spaces.'),
  ];

  $form['event_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Event Settings'),
    '#required' => TRUE,
  ];

  $form['event_settings']['start_date'] = [
    '#type' => 'date_popup',
    '#title' => t('Start Date/Time'),
    '#date_format' => 'm/d/Y h:ia',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+1',
    '#date_timezone' => date_default_timezone(),
    '#required' => TRUE,
  ];

  $form['event_settings']['end_date'] = [
    '#type' => 'date_popup',
    '#title' => t('End Date/Time'),
    '#date_format' => 'm/d/Y h:ia',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+1',
    '#date_timezone' => date_default_timezone(),
    '#required' => TRUE,
  ];

  $form['event_settings']['location'] = [
    '#type' => 'textfield',
    '#title' => t('Event Location'),
    '#required' => TRUE,
    '#description' => t('Use a list of previous locations or input your own.'),
    '#autocomplete_path' => 'events/add/location/autocomplete',
  ];

  $form['photo'] = [
    '#type' => 'managed_file',
    '#title' => t('Event Photo'),
    '#required' => FALSE,
    '#upload_location' => 'temporary://image_uploads/',
    '#upload_validators' => [
      'file_validate_extensions' => ['png gif jpg jpeg'],
    ],
  ];

  $form['body'] = [
    '#type' => 'textarea',
    '#title' => t('Event Description'),
    '#required' => TRUE,
  ];

  $form['taxonomy'] = [
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Tags'),
    '#tree' => TRUE,
  ];

  [$parents, $terms] = _mysite_event_form_terms();

  $options = [];
  foreach ($terms as $term) {
    $options[$term->parent][$term->tid] = $term->name;
  }
  foreach ($parents as $parent => $name) {
    $form["taxonomy"]["terms{$parent}"] = [
      '#type' => 'checkboxes',
      '#title' => t($name),
      '#options' => $options[$parent],
    ];
  }

  $form['listing_info'] = [
    '#type' => 'fieldset',
    '#title' => t('Optional Listing Details'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];

  $form['listing_info']['cut_line'] = [
    '#type' => 'textfield',
    '#title' => t('Event Summary'),
    '#maxlength' => 255,
    '#description' => t('
      This line will appear under the title in some listings.
      <strong>Do not repeat the title in the summary.</strong>
    '),
  ];

  $form['listing_info']['alt_link'] = [
    '#type' => 'textfield',
    '#title' => t('External Link'),
    '#maxlength' => 128,
    '#description' => t('
      <p>If this event has a Zoom link or details already exist on another page,
      you may include the link here. This event will link directly to
      the external page from all listings.</p>
      <p><strong>Do not use this space to request a specific link for your event.
      The link specified here <em>must</em> already exist.</strong></p>
    '),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Validate new event form submission.
 */
function mysite_event_form_validate($form, &$form_state) {
  // @todo LIVE FORM VALIDATION TO IMPROVE USER EXPERIENCE?
  if (strtotime($form_state['values']['start_date']) > strtotime($form_state['values']['end_date'])) {
    form_set_error('end_date', t('Your event start date must come before your end date.'));
  }

  if (!empty($form_state['values']['alt_link'])) {
    $response = drupal_http_request($form_state['values']['alt_link']);
    if ($response->code == 404) {
      form_set_error('alt_link', t('External link not found.'));
    }
    elseif (!in_array($response->code, [200, 401, 403])) {
      form_set_error(
        'alt_link',
        t(
          'External link error %r. Please use a different link.',
          ['%r' => $response->code]
        )
      );
    }
  }
}

/**
 * Process event form submissions.
 */
function mysite_event_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $node = new StdClass();
  $node->type = 'news';
  $node->status = 0;
  $node->promote = 0;
  $node->language = LANGUAGE_NONE;

  node_object_prepare($node);

  $node->title = $values['title'];
  $node->field_news_event_date[LANGUAGE_NONE][0] = [
    'value' => strtotime($values['start_date']),
    'value2' => strtotime($values['end_date']),
    'rrule' => NULL,
    'timezone' => drupal_get_user_timezone(),
    'timezone_db' => date_default_timezone(),
    'date_type' => 'datestamp',
  ];
  $node->field_news_event_location[LANGUAGE_NONE][0]['value'] = $values['location'];
  $node->body[LANGUAGE_NONE][0] = ['value' => $values['body'], 'format' => 'markdown'];

  $terms = [];
  foreach ($values['taxonomy'] as $field) {
    foreach ($field as $tid) {
      if ($tid > 0) {
        $terms[] = ['tid' => $tid];
      }
    }
  }

  if (!empty($terms)) {
    $node->field_news_tags[LANGUAGE_NONE] = $terms;
  }

  if (!empty($values['photo'])) {
    $node->field_news_photo[LANGUAGE_NONE][0]['fid'] = $values['photo'];
    $file = file_load($values['photo']);
  }

  if (!empty($values['cut_line'])) {
    $node->field_news_cut_line[LANGUAGE_NONE][0]['value'] = $values['cut_line'];
  }

  if (!empty($values['alt_link'])) {
    $node->field_news_alt_link[LANGUAGE_NONE][0]['url'] = $values['alt_link'];
  }

  node_save($node);

  if ($node->nid) {
    if (isset($file)) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
    }
    drupal_goto("node/{$node->nid}");
  }
}

/**
 * Get taxonomy terms options for the events form.
 */
function _mysite_event_form_terms() {
  $query = db_select("taxonomy_term_data", "t");
  $query->fields("t", ["tid", "name"]);
  $query->join("taxonomy_term_hierarchy", "h", "h.tid = t.tid");
  $query->condition("t.vid", 1);
  $query->condition("h.parent", 0);
  $query->orderBy("t.weight");
  $parents = $query->execute()->fetchAllKeyed();

  $query = db_select("taxonomy_term_data", "t");
  $query->fields("t", ["tid", "name"]);
  $query->join("taxonomy_term_hierarchy", "h", "h.tid = t.tid");
  $query->fields("h", ["parent"]);
  $query->condition("t.vid", 1);
  $query->condition("h.parent", 0, ">");
  $query->orderBy("t.weight");
  $terms = $query->execute()->fetchAll();
  return [$parents, $terms];
}
