<?php

/**
 * News Retire Form
 */
function mysite_news_retire_form($form, &$form_state, $node) {

  $news_expire_service = NewsExpireService::create();

  $type_name = node_type_get_name($node);
  drupal_set_title(t('Retire @type content <em>@title</em>', [
    '@type' => $type_name,
    '@title' => $node->title,
  ]), PASS_THROUGH);

  $form['node'] = [
    '#type' => 'value',
    '#value' => $node,
  ];

  $form['retire_date'] = [
    '#type' => 'date_popup',
    '#title' => t('Retire Date'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+2',
    '#date_timezone' => date_default_timezone(),
    '#default_value' => !empty($node->field_news_retire_date[LANGUAGE_NONE][0]['value'])
      ? format_date($node->field_news_retire_date[LANGUAGE_NONE][0]['value'], 'custom', 'Y-m-d')
      : NULL,
    '#description' => t('
      <p>Enter new retire date or use <strong><em>Retire now</em></strong> button below to set this automatically.</p>
      <p>Retired content will be demoted from primary landing pages and displayed in archive only.</p>
    '),
  ];

  $promote_flags = [];
  if (!empty($node->field_news_promote[LANGUAGE_NONE])) {
    foreach ($node->field_news_promote[LANGUAGE_NONE] as $flag) {
      $promote_flags[$flag['value']] = $flag['value'];
    }
  }

  $form['field_news_promote'] = [
    '#type' => 'checkboxes',
    '#options' => field_info_field('field_news_promote')['settings']['allowed_values'],
    '#title' => t('Promote to Landing Pages'),
    '#default_value' => $promote_flags,
  ];

  $flags = [];

  $flags['evergreen'] = (isset($node->field_evergreen[LANGUAGE_NONE][0]['value']) && $node->field_evergreen[LANGUAGE_NONE][0]['value'])
    ? 'evergreen'
    : NULL;

  $form['flags'] = [
    '#type' => 'checkboxes',
    '#title' => t('Flags'),
    '#options' => ['evergreen' => t('Evergreen content')],
    '#default_value' => $flags,
  ];

  $form['expire_date'] = [
    '#type' => 'date_popup',
    '#title' => t('Expire Date'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+2',
    '#date_timezone' => date_default_timezone(),
    '#default_value' => !empty($node->field_expire_content[LANGUAGE_NONE][0]['value'])
      ? format_date($node->field_expire_content[LANGUAGE_NONE][0]['value'], 'custom', 'Y-m-d')
      : NULL,
    '#description' => t('Enter (optional) date when this content should be <em>deleted</em>.'),
  ];

  $form['update'] = [
    '#type' => 'submit',
    '#value' => t('Update info'),
  ];

  $form['retire_now'] = [
    '#type' => 'submit',
    '#value' => t('Retire now'),
  ];

  return $form;
}

/**
 * News Retire Form validation handler.
 */
function mysite_news_retire_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['retire_date']) && !strtotime($form_state['values']['retire_date'])) {
    form_set_error('retire_date', t('Cannot interpret retire date value.'));
  }
}

/**
 * News Retire Form submit handler.
 */
function mysite_news_retire_form_submit($form, &$form_state) {

  $node = $form_state['values']['node'];
  $op = ($form_state['values']['op'] == t('Retire now')) ? 'retire' : 'update';
  $values = [];
  foreach(['retire_date', 'field_news_promote', 'flags', 'expire_date'] as $field) {
    $values[$field] = $form_state['values'][$field];
  }
  NewsExpireService::create()->doUpdate($node, $op, $values);
}
