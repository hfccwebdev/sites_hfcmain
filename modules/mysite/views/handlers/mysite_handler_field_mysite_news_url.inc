<?php

class mysite_handler_field_mysite_news_url extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      return url($value, ['absolute' => TRUE]);
    }
    else {
      if (isset($values->nid)) {
        $nid = $values->nid;
        return url("node/$nid", ['absolute' => TRUE]);
      }
    }
  }
}
