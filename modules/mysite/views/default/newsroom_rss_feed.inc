<?php

$view = new view();
$view->name = 'newsroom_rss_feed';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Newsroom RSS Feed';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Henry Ford College News';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'time';
$handler->display->display_options['cache']['results_lifespan'] = '3600';
$handler->display->display_options['cache']['results_lifespan_custom'] = '0';
$handler->display->display_options['cache']['output_lifespan'] = '3600';
$handler->display->display_options['cache']['output_lifespan_custom'] = '0';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Article/Event Short Title */
$handler->display->display_options['fields']['field_news_event_short_title']['id'] = 'field_news_event_short_title';
$handler->display->display_options['fields']['field_news_event_short_title']['table'] = 'field_data_field_news_event_short_title';
$handler->display->display_options['fields']['field_news_event_short_title']['field'] = 'field_news_event_short_title';
$handler->display->display_options['fields']['field_news_event_short_title']['label'] = '';
$handler->display->display_options['fields']['field_news_event_short_title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_short_title']['empty'] = '[title]';
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['path']['absolute'] = TRUE;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['exclude'] = TRUE;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
$handler->display->display_options['fields']['body']['settings'] = array(
  'trim_length' => '300',
);
/* Field: Content: News Summary */
$handler->display->display_options['fields']['field_news_cut_line']['id'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['table'] = 'field_data_field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['field'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['label'] = '';
$handler->display->display_options['fields']['field_news_cut_line']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_cut_line']['empty'] = '[body]';
/* Field: Creator */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['ui_name'] = 'Creator';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'Henry Ford College';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
/* Field: Field: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['label'] = '';
$handler->display->display_options['fields']['field_news_release_date']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_news_release_date']['element_type'] = '0';
$handler->display->display_options['fields']['field_news_release_date']['element_label_type'] = '0';
$handler->display->display_options['fields']['field_news_release_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_release_date']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_news_release_date']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'rfc_2822_formatted_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
/* Field: Content: Node UUID */
$handler->display->display_options['fields']['uuid']['id'] = 'uuid';
$handler->display->display_options['fields']['uuid']['table'] = 'node';
$handler->display->display_options['fields']['uuid']['field'] = 'uuid';
$handler->display->display_options['fields']['uuid']['label'] = '';
$handler->display->display_options['fields']['uuid']['exclude'] = TRUE;
$handler->display->display_options['fields']['uuid']['element_label_colon'] = FALSE;
/* Sort criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['granularity'] = 'second';
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  0 => '138',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid_1']['id'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid_1']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid_1']['operator'] = 'not';
$handler->display->display_options['filters']['field_news_tags_tid_1']['value'] = array(
  0 => '113',
  1 => '114',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator_id'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['label'] = 'Tags (field_news_tags)';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['identifier'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['vocabulary'] = 'tags';

/* Display: Public Feed */
$handler = $view->new_display('feed', 'Public Feed', 'feed_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'rss';
$handler->display->display_options['style_options']['description'] = 'The latest news from HFC.';
$handler->display->display_options['row_plugin'] = 'rss_fields';
$handler->display->display_options['row_options']['title_field'] = 'field_news_event_short_title';
$handler->display->display_options['row_options']['link_field'] = 'path';
$handler->display->display_options['row_options']['description_field'] = 'body';
$handler->display->display_options['row_options']['creator_field'] = 'nothing';
$handler->display->display_options['row_options']['date_field'] = 'field_news_release_date';
$handler->display->display_options['row_options']['guid_field_options'] = array(
  'guid_field' => 'uuid',
  'guid_field_is_permalink' => 0,
);
$handler->display->display_options['path'] = 'news/rss';
$handler->display->display_options['sitename_title'] = 0;

/* Display: Portal Feed */
$handler = $view->new_display('feed', 'Portal Feed', 'feed_2');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'rss';
$handler->display->display_options['style_options']['description'] = 'The latest news from HFC.';
$handler->display->display_options['row_plugin'] = 'rss_fields';
$handler->display->display_options['row_options']['title_field'] = 'field_news_event_short_title';
$handler->display->display_options['row_options']['link_field'] = 'path';
$handler->display->display_options['row_options']['description_field'] = 'field_news_cut_line';
$handler->display->display_options['row_options']['creator_field'] = 'nothing';
$handler->display->display_options['row_options']['date_field'] = 'field_news_release_date';
$handler->display->display_options['row_options']['guid_field_options'] = array(
  'guid_field' => 'uuid',
  'guid_field_is_permalink' => 0,
);
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['granularity'] = 'second';
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  0 => '253',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid_1']['id'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid_1']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid_1']['operator'] = 'not';
$handler->display->display_options['filters']['field_news_tags_tid_1']['value'] = array(
  0 => '113',
  1 => '114',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator_id'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['label'] = 'Tags (field_news_tags)';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['identifier'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['vocabulary'] = 'tags';
$handler->display->display_options['path'] = 'news/portal.rss';
$handler->display->display_options['sitename_title'] = 0;
