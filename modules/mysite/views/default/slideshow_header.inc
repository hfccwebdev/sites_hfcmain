<?php

$view = new view();
$view->name = 'slideshow_header';
$view->description = 'Sets the CSS background for header if Billboard image available';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Slideshow Header';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Billboard Carousel';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Relationship: Content: Header Image (field_header_image:fid) */
$handler->display->display_options['relationships']['field_header_image_fid']['id'] = 'field_header_image_fid';
$handler->display->display_options['relationships']['field_header_image_fid']['table'] = 'field_data_field_header_image';
$handler->display->display_options['relationships']['field_header_image_fid']['field'] = 'field_header_image_fid';
$handler->display->display_options['relationships']['field_header_image_fid']['label'] = 'header_image';
/* Field: File: Path */
$handler->display->display_options['fields']['uri']['id'] = 'uri';
$handler->display->display_options['fields']['uri']['table'] = 'file_managed';
$handler->display->display_options['fields']['uri']['field'] = 'uri';
$handler->display->display_options['fields']['uri']['relationship'] = 'field_header_image_fid';
$handler->display->display_options['fields']['uri']['label'] = '';
$handler->display->display_options['fields']['uri']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['uri']['alter']['text'] = '#header-image { background-image: url([uri]); }';
$handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['uri']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['uri']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
/* Sort criterion: Content: Sticky status */
$handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
$handler->display->display_options['sorts']['sticky']['table'] = 'node';
$handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
$handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
/* Sort criterion: Global: Random */
$handler->display->display_options['sorts']['random']['id'] = 'random';
$handler->display->display_options['sorts']['random']['table'] = 'views';
$handler->display->display_options['sorts']['random']['field'] = 'random';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

/* Display: Front Page */
$handler = $view->new_display('block', 'Front Page', 'block_2');
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Content: Photo (field_photo:fid) */
$handler->display->display_options['relationships']['field_photo_fid']['id'] = 'field_photo_fid';
$handler->display->display_options['relationships']['field_photo_fid']['table'] = 'field_data_field_photo';
$handler->display->display_options['relationships']['field_photo_fid']['field'] = 'field_photo_fid';
$handler->display->display_options['relationships']['field_photo_fid']['label'] = 'field_photo';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
/* Field: File: Path */
$handler->display->display_options['fields']['uri']['id'] = 'uri';
$handler->display->display_options['fields']['uri']['table'] = 'file_managed';
$handler->display->display_options['fields']['uri']['field'] = 'uri';
$handler->display->display_options['fields']['uri']['relationship'] = 'field_photo_fid';
$handler->display->display_options['fields']['uri']['label'] = '';
$handler->display->display_options['fields']['uri']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['uri']['alter']['text'] = '#header-[nid] { background-image: url([uri]); }';
$handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['uri']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
/* Field: Content: News Summary */
$handler->display->display_options['fields']['field_news_cut_line']['id'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['table'] = 'field_data_field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['field'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['label'] = '';
$handler->display->display_options['fields']['field_news_cut_line']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_cut_line']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Offsite Link */
$handler->display->display_options['fields']['field_news_alt_link']['id'] = 'field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['table'] = 'field_data_field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['field'] = 'field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['label'] = '';
$handler->display->display_options['fields']['field_news_alt_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_alt_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_alt_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_news_alt_link']['type'] = 'link_plain';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<div id="header-[nid]" class="header-image">
<a href="[field_news_alt_link]" class="no-link">
<div class="billboard-caption">
<h2>[title]</h2>[field_news_cut_line]
</div>
</a>
</div>';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'front_page_image' => 'front_page_image',
);
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
/* Filter criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
$handler->display->display_options['block_description'] = 'Front Page Header Slideshow';
