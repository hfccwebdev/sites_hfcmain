<?php

$view = new view();
$view->name = 'editor_tag_block';
$view->description = 'Block shown only to editors listing tags on page.';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Editor Tag Block';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Tags';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  4 => '4',
  7 => '7',
  18 => '18',
  14 => '14',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Field: Tags */
$handler->display->display_options['fields']['field_news_tags']['id'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['field'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['label'] = '';
$handler->display->display_options['fields']['field_news_tags']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_tags']['type'] = 'taxonomy_term_reference_plain';
$handler->display->display_options['fields']['field_news_tags']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_tags']['multi_type'] = 'ul';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block');
$handler->display->display_options['block_description'] = 'Tag list';
