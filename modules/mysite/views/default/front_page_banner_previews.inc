<?php

$view = new view();
$view->name = 'front_page_banner_previews';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Front Page Banner Previews';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Front Page Banner Previews';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access content overview';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h2';
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
$handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo']['label'] = '';
$handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo']['settings'] = array(
  'image_style' => 'mini_header_gallery_600x220',
  'image_link' => '',
);
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['label'] = '';
$handler->display->display_options['fields']['status']['exclude'] = TRUE;
$handler->display->display_options['fields']['status']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['status']['alter']['text'] = 'Published';
$handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['status']['empty'] = '<strong>Unpublished</strong>';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Field: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['label'] = '';
$handler->display->display_options['fields']['field_news_release_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_release_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_release_date']['empty'] = '<strong>No release date</strong>';
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'custom',
  'custom_date_format' => 'D, n/j/Y',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Field: Retire Date */
$handler->display->display_options['fields']['field_news_retire_date']['id'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['field'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['label'] = '';
$handler->display->display_options['fields']['field_news_retire_date']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_retire_date']['alter']['text'] = '[field_news_release_date] - [field_news_retire_date] ([status])';
$handler->display->display_options['fields']['field_news_retire_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_retire_date']['empty'] = '[field_news_release_date] ([status])';
$handler->display->display_options['fields']['field_news_retire_date']['settings'] = array(
  'format_type' => 'custom',
  'custom_date_format' => 'D, n/j/Y',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'front_page_image' => 'front_page_image',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/news/banner';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Banners';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
