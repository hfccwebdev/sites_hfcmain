<?php

$view = new view();
$view->name = 'program_inventory';
$view->description = 'Inventory of program pages';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Program Inventory';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Program Inventory';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'node';
$handler->display->display_options['fields']['type']['field'] = 'type';
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'short_date_only';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Sort criterion: Content: Type */
$handler->display->display_options['sorts']['type']['id'] = 'type';
$handler->display->display_options['sorts']['type']['table'] = 'node';
$handler->display->display_options['sorts']['type']['field'] = 'type';
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'program' => 'program',
  'program_para' => 'program_para',
);
$handler->display->display_options['filters']['type']['group'] = 1;

/* Display: Programs Page */
$handler = $view->new_display('page', 'Programs Page', 'page_inventory');
$handler->display->display_options['path'] = 'admin/content/programs/inventory';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Program Inventory';
$handler->display->display_options['menu']['weight'] = '-10';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'tab';
$handler->display->display_options['tab_options']['title'] = 'Programs';
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Missing Header Image */
$handler = $view->new_display('page', 'Missing Header Image', 'page_missing_header');
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'program' => 'program',
  'program_para' => 'program_para',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Header Image (field_header_image:fid) */
$handler->display->display_options['filters']['field_header_image_fid']['id'] = 'field_header_image_fid';
$handler->display->display_options['filters']['field_header_image_fid']['table'] = 'field_data_field_header_image';
$handler->display->display_options['filters']['field_header_image_fid']['field'] = 'field_header_image_fid';
$handler->display->display_options['filters']['field_header_image_fid']['operator'] = 'empty';
$handler->display->display_options['path'] = 'admin/content/programs/missing-header';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Missing Header';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Courses Page */
$handler = $view->new_display('page', 'Courses Page', 'page_courses');
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_related_node_target_id']['id'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['table'] = 'field_data_field_related_node';
$handler->display->display_options['relationships']['field_related_node_target_id']['field'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['label'] = 'Related Node';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Field: Related Node */
$handler->display->display_options['fields']['field_related_node']['id'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['table'] = 'field_data_field_related_node';
$handler->display->display_options['fields']['field_related_node']['field'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['label'] = 'Program';
$handler->display->display_options['fields']['field_related_node']['settings'] = array(
  'bypass_access' => 0,
  'link' => 1,
);
$handler->display->display_options['fields']['field_related_node']['delta_offset'] = '0';
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['relationship'] = 'field_related_node_target_id';
$handler->display->display_options['fields']['status']['label'] = 'Program Published';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'short_date_only';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'course' => 'course',
);
$handler->display->display_options['filters']['type']['group'] = 1;
$handler->display->display_options['path'] = 'admin/content/programs/courses';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Courses';
$handler->display->display_options['menu']['weight'] = '2';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Highlights Page */
$handler = $view->new_display('page', 'Highlights Page', 'page_highlights');
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_related_node_target_id']['id'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['table'] = 'field_data_field_related_node';
$handler->display->display_options['relationships']['field_related_node_target_id']['field'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['label'] = 'Related Program';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Front Page Highlight */
$handler->display->display_options['fields']['field_front_page_highlight']['id'] = 'field_front_page_highlight';
$handler->display->display_options['fields']['field_front_page_highlight']['table'] = 'field_data_field_front_page_highlight';
$handler->display->display_options['fields']['field_front_page_highlight']['field'] = 'field_front_page_highlight';
$handler->display->display_options['fields']['field_front_page_highlight']['delta_offset'] = '0';
/* Field: Field: Related Node */
$handler->display->display_options['fields']['field_related_node']['id'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['table'] = 'field_data_field_related_node';
$handler->display->display_options['fields']['field_related_node']['field'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['label'] = 'Related Content';
$handler->display->display_options['fields']['field_related_node']['settings'] = array(
  'bypass_access' => 0,
  'link' => 1,
);
$handler->display->display_options['fields']['field_related_node']['delta_offset'] = '0';
/* Field: Content: Retire Date */
$handler->display->display_options['fields']['field_retire_date']['id'] = 'field_retire_date';
$handler->display->display_options['fields']['field_retire_date']['table'] = 'field_data_field_retire_date';
$handler->display->display_options['fields']['field_retire_date']['field'] = 'field_retire_date';
$handler->display->display_options['fields']['field_retire_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['relationship'] = 'field_related_node_target_id';
$handler->display->display_options['fields']['status']['label'] = 'Program Published';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'short_date_only';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title_1']['id'] = 'title_1';
$handler->display->display_options['sorts']['title_1']['table'] = 'node';
$handler->display->display_options['sorts']['title_1']['field'] = 'title';
$handler->display->display_options['sorts']['title_1']['relationship'] = 'field_related_node_target_id';
/* Sort criterion: Content: Front Page Highlight (field_front_page_highlight) */
$handler->display->display_options['sorts']['field_front_page_highlight_value']['id'] = 'field_front_page_highlight_value';
$handler->display->display_options['sorts']['field_front_page_highlight_value']['table'] = 'field_data_field_front_page_highlight';
$handler->display->display_options['sorts']['field_front_page_highlight_value']['field'] = 'field_front_page_highlight_value';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Sort criterion: Content: Type */
$handler->display->display_options['sorts']['type']['id'] = 'type';
$handler->display->display_options['sorts']['type']['table'] = 'node';
$handler->display->display_options['sorts']['type']['field'] = 'type';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'highlight' => 'highlight',
);
$handler->display->display_options['filters']['type']['group'] = 1;
$handler->display->display_options['path'] = 'admin/content/programs/highlights';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Highlights';
$handler->display->display_options['menu']['weight'] = '4';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: People Page */
$handler = $view->new_display('page', 'People Page', 'page_people');
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_related_node_target_id']['id'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['table'] = 'field_data_field_related_node';
$handler->display->display_options['relationships']['field_related_node_target_id']['field'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['label'] = 'Related Node';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Field: Related Node */
$handler->display->display_options['fields']['field_related_node']['id'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['table'] = 'field_data_field_related_node';
$handler->display->display_options['fields']['field_related_node']['field'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['label'] = 'Related Content';
$handler->display->display_options['fields']['field_related_node']['empty'] = '<strong><em>NONE</em></strong>';
$handler->display->display_options['fields']['field_related_node']['settings'] = array(
  'bypass_access' => 0,
  'link' => 1,
);
$handler->display->display_options['fields']['field_related_node']['delta_offset'] = '0';
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['relationship'] = 'field_related_node_target_id';
$handler->display->display_options['fields']['status']['label'] = 'Program Published';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'short_date_only';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status or admin user */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'faculty' => 'faculty',
);
$handler->display->display_options['filters']['type']['group'] = 1;
$handler->display->display_options['path'] = 'admin/content/programs/people';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'People';
$handler->display->display_options['menu']['weight'] = '6';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
