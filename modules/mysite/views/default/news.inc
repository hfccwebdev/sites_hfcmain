<?php

$view = new view();
$view->name = 'news';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Top news blocks';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['css_class'] = 'top-story news-promoted-feature';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'custom_url';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['bef'] = array(
  'general' => array(
    'input_required' => 0,
    'text_input_required' => array(
      'text_input_required' => array(
        'value' => 'Select any filter and click on Apply to see results',
        'format' => 'markdown',
      ),
    ),
    'allow_secondary' => 0,
    'secondary_label' => 'Advanced options',
    'secondary_collapse_override' => '0',
  ),
  'field_news_tags_tid_1' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 0,
      'bef_collapsible' => 0,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
          1 => 'vocabulary',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
);
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'feature';
/* Sort criterion: Content: News Order Front (field_news_order_front) */
$handler->display->display_options['sorts']['field_news_order_front_value']['id'] = 'field_news_order_front_value';
$handler->display->display_options['sorts']['field_news_order_front_value']['table'] = 'field_data_field_news_order_front';
$handler->display->display_options['sorts']['field_news_order_front_value']['field'] = 'field_news_order_front_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_front_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'front';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['group'] = 1;
/* Filter criterion: Content: Photo (field_news_photo:fid) */
$handler->display->display_options['filters']['field_news_photo_fid']['id'] = 'field_news_photo_fid';
$handler->display->display_options['filters']['field_news_photo_fid']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['filters']['field_news_photo_fid']['field'] = 'field_news_photo_fid';
$handler->display->display_options['filters']['field_news_photo_fid']['operator'] = 'not empty';
$handler->display->display_options['filters']['field_news_photo_fid']['group'] = 1;

/* Display: Front Top News Block */
$handler = $view->new_display('block', 'Front Top News Block', 'block_front_news_top');
$handler->display->display_options['block_description'] = 'Top News Block Front Page';

/* Display: Public Top News Block */
$handler = $view->new_display('block', 'Public Top News Block', 'block_public_news_top');
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Public (field_news_order_public) */
$handler->display->display_options['sorts']['field_news_order_public_value']['id'] = 'field_news_order_public_value';
$handler->display->display_options['sorts']['field_news_order_public_value']['table'] = 'field_data_field_news_order_public';
$handler->display->display_options['sorts']['field_news_order_public_value']['field'] = 'field_news_order_public_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_public_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'public';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'Top News Block Public News';

/* Display: Public Secondary News Block */
$handler = $view->new_display('block', 'Public Secondary News Block', 'block_public_news_secondary');
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['css_class'] = 'top-story news-promoted-feature highlight-header gray';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '1';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Public (field_news_order_public) */
$handler->display->display_options['sorts']['field_news_order_public_value']['id'] = 'field_news_order_public_value';
$handler->display->display_options['sorts']['field_news_order_public_value']['table'] = 'field_data_field_news_order_public';
$handler->display->display_options['sorts']['field_news_order_public_value']['field'] = 'field_news_order_public_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_public_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'public';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'Secondary News Block Public News';

/* Display: Student Top News Block */
$handler = $view->new_display('block', 'Student Top News Block', 'block_student_news_top');
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Student (field_news_order_student) */
$handler->display->display_options['sorts']['field_news_order_student_value']['id'] = 'field_news_order_student_value';
$handler->display->display_options['sorts']['field_news_order_student_value']['table'] = 'field_data_field_news_order_student';
$handler->display->display_options['sorts']['field_news_order_student_value']['field'] = 'field_news_order_student_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_student_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'student';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'Top News Block Student News';

/* Display: Employee Top News Block */
$handler = $view->new_display('block', 'Employee Top News Block', 'block_employee_news_top');
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['css_class'] = 'top-story news-promoted-feature highlight-header gray';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Employee (field_news_order_employee) */
$handler->display->display_options['sorts']['field_news_order_employee_value']['id'] = 'field_news_order_employee_value';
$handler->display->display_options['sorts']['field_news_order_employee_value']['table'] = 'field_data_field_news_order_employee';
$handler->display->display_options['sorts']['field_news_order_employee_value']['field'] = 'field_news_order_employee_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_employee_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'employee';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'Top News Block Employee News';
