<?php

$view = new view();
$view->name = 'news_grid_for_all_pages';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'News grid for primary landing pages';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['css_class'] = 'highlight-header-wide news-cards-grid';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'page_6';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['bef'] = array(
  'general' => array(
    'input_required' => 0,
    'text_input_required' => array(
      'text_input_required' => array(
        'value' => 'Select any filter and click on Apply to see results',
        'format' => 'markdown',
      ),
    ),
    'allow_secondary' => 0,
    'secondary_label' => 'Advanced options',
    'secondary_collapse_override' => '0',
  ),
  'body_value' => array(
    'bef_format' => 'default',
    'more_options' => array(
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
  'field_news_tags_tid' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 0,
      'bef_collapsible' => 0,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
          1 => 'vocabulary',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
);
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '4';
$handler->display->display_options['pager']['options']['offset'] = '1';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'newsgrid';
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['label'] = 'photo only';
$handler->display->display_options['fields']['field_news_photo']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date_1']['id'] = 'field_news_event_date_1';
$handler->display->display_options['fields']['field_news_event_date_1']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['label'] = 'if photo';
$handler->display->display_options['fields']['field_news_event_date_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date_1']['alter']['text'] = '<div class="event-photo-card">
<div class="event-date">[field_news_event_date_1]</div>
[field_news_photo]
</div>';
$handler->display->display_options['fields']['field_news_event_date_1']['empty'] = '[field_news_photo]';
$handler->display->display_options['fields']['field_news_event_date_1']['settings'] = array(
  'format_type' => 'short_calendar_with_time',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_news_event_date_1']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_news_event_date_1']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_event_date_1']['separator'] = '';
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = 'if no photo';
$handler->display->display_options['fields']['field_news_event_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['alter']['text'] = '<div class="event-no-photo-card">
<div class="event-date">[field_news_event_date]</div>
</div>';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'short_calendar_with_time',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_news_event_date']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_event_date']['delta_first_last'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['separator'] = ' – ';
/* Field: Field: Paragraphs */
$handler->display->display_options['fields']['field_paragraphs']['id'] = 'field_paragraphs';
$handler->display->display_options['fields']['field_paragraphs']['table'] = 'field_data_field_paragraphs';
$handler->display->display_options['fields']['field_paragraphs']['field'] = 'field_paragraphs';
$handler->display->display_options['fields']['field_paragraphs']['label'] = '';
$handler->display->display_options['fields']['field_paragraphs']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_paragraphs']['alter']['max_length'] = '200';
$handler->display->display_options['fields']['field_paragraphs']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_paragraphs']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['field_paragraphs']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_paragraphs']['settings'] = array(
  'view_mode' => 'full',
);
$handler->display->display_options['fields']['field_paragraphs']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_paragraphs']['separator'] = '. ';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['exclude'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['max_length'] = '200';
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['empty'] = '[field_paragraphs]';
/* Field: Content: Title */
$handler->display->display_options['fields']['title_1']['id'] = 'title_1';
$handler->display->display_options['fields']['title_1']['table'] = 'node';
$handler->display->display_options['fields']['title_1']['field'] = 'title';
$handler->display->display_options['fields']['title_1']['label'] = 'read more';
$handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['title_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title_1']['alter']['text'] = '<span class="hidden">[title_1] </span>Read More';
$handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo_1']['id'] = 'field_news_photo_1';
$handler->display->display_options['fields']['field_news_photo_1']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo_1']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo_1']['label'] = '';
$handler->display->display_options['fields']['field_news_photo_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_photo_1']['alter']['text'] = '[field_news_event_date_1]';
$handler->display->display_options['fields']['field_news_photo_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_photo_1']['empty'] = '[field_news_event_date]';
$handler->display->display_options['fields']['field_news_photo_1']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo_1']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: News Summary */
$handler->display->display_options['fields']['field_news_cut_line']['id'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['table'] = 'field_data_field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['field'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['label'] = '';
$handler->display->display_options['fields']['field_news_cut_line']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_cut_line']['alter']['text'] = '<h3>[title]</h3>
[field_news_cut_line]';
$handler->display->display_options['fields']['field_news_cut_line']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_cut_line']['empty'] = '<h3>[title]</h3>
[body]';
/* Field: Content: Offsite Link */
$handler->display->display_options['fields']['field_news_alt_link']['id'] = 'field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['table'] = 'field_data_field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['field'] = 'field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['label'] = '';
$handler->display->display_options['fields']['field_news_alt_link']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_alt_link']['alter']['text'] = '<span class="hidden">[title] </span>Read More';
$handler->display->display_options['fields']['field_news_alt_link']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_news_alt_link']['alter']['path'] = '[field_news_alt_link-url]';
$handler->display->display_options['fields']['field_news_alt_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_alt_link']['empty'] = '[title_1]';
$handler->display->display_options['fields']['field_news_alt_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_news_alt_link']['type'] = 'link_plain';
/* Sort criterion: Content: News Order Front (field_news_order_front) */
$handler->display->display_options['sorts']['field_news_order_front_value']['id'] = 'field_news_order_front_value';
$handler->display->display_options['sorts']['field_news_order_front_value']['table'] = 'field_data_field_news_order_front';
$handler->display->display_options['sorts']['field_news_order_front_value']['field'] = 'field_news_order_front_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_front_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'front';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';

/* Display: News Front Grid */
$handler = $view->new_display('block', 'News Front Grid', 'block_news_front_grid');
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['label'] = 'more link';
$handler->display->display_options['footer']['area']['content'] = '<a href="/news" class="hfc-button">More News</a>';
$handler->display->display_options['footer']['area']['format'] = 'markdown';
$handler->display->display_options['block_description'] = 'News grid front';

/* Display: Public News Grid */
$handler = $view->new_display('block', 'Public News Grid', 'block_news_public_grid');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '12';
$handler->display->display_options['pager']['options']['offset'] = '2';
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['content'] = '<a href="/news/archive" class="hfc-button">MORE NEWS AND ARCHIVES</a>';
$handler->display->display_options['footer']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Public (field_news_order_public) */
$handler->display->display_options['sorts']['field_news_order_public_value']['id'] = 'field_news_order_public_value';
$handler->display->display_options['sorts']['field_news_order_public_value']['table'] = 'field_data_field_news_order_public';
$handler->display->display_options['sorts']['field_news_order_public_value']['field'] = 'field_news_order_public_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_public_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'public';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'News grid public';

/* Display: Student News Grid */
$handler = $view->new_display('block', 'Student News Grid', 'block_news_student_grid');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '12';
$handler->display->display_options['pager']['options']['offset'] = '1';
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['content'] = '<a href="/news/archive" class="hfc-button">MORE NEWS AND ARCHIVES</a>';
$handler->display->display_options['footer']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Student (field_news_order_student) */
$handler->display->display_options['sorts']['field_news_order_student_value']['id'] = 'field_news_order_student_value';
$handler->display->display_options['sorts']['field_news_order_student_value']['table'] = 'field_data_field_news_order_student';
$handler->display->display_options['sorts']['field_news_order_student_value']['field'] = 'field_news_order_student_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_student_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'student';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'News grid student';

/* Display: Employee News Grid */
$handler = $view->new_display('block', 'Employee News Grid', 'block_news_employee_grid');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '32';
$handler->display->display_options['pager']['options']['offset'] = '1';
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['content'] = '<a href="/news/archive" class="hfc-button">MORE NEWS AND ARCHIVES</a>';
$handler->display->display_options['footer']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Employee (field_news_order_employee) */
$handler->display->display_options['sorts']['field_news_order_employee_value']['id'] = 'field_news_order_employee_value';
$handler->display->display_options['sorts']['field_news_order_employee_value']['table'] = 'field_data_field_news_order_employee';
$handler->display->display_options['sorts']['field_news_order_employee_value']['field'] = 'field_news_order_employee_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Draggableviews: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'news_promoted_sorting:page_news_employee_sort';
$handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'employee';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'News grid employee';
