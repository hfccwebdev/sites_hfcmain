<?php

$view = new view();
$view->name = 'explore_all_program';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Explore All Programs';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Explore All Programs (demo)';
$handler->display->display_options['css_class'] = 'view-flex cols-3';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  17 => '17',
  4 => '4',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: metamajor */
$handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
$handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
$handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
$handler->display->display_options['relationships']['term_node_tid']['ui_name'] = 'metamajor';
$handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
  'meta_majors' => 'meta_majors',
  'student_services_faq' => 0,
  'tags' => 0,
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h2';
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
$handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
$handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
$handler->display->display_options['fields']['field_photo']['label'] = '';
$handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_photo']['empty'] = '<strong><em>no photo</em></strong>';
$handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_photo']['settings'] = array(
  'image_style' => 'gallery_image_wide_600x440',
  'image_link' => 'content',
);
/* Field: Content: Header Text */
$handler->display->display_options['fields']['field_header_text']['id'] = 'field_header_text';
$handler->display->display_options['fields']['field_header_text']['table'] = 'field_data_field_header_text';
$handler->display->display_options['fields']['field_header_text']['field'] = 'field_header_text';
$handler->display->display_options['fields']['field_header_text']['label'] = '';
$handler->display->display_options['fields']['field_header_text']['element_label_colon'] = FALSE;
/* Field: Metamajor */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'term_node_tid';
$handler->display->display_options['fields']['name']['ui_name'] = 'Metamajor';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['name']['element_wrapper_class'] = 'field-explore-metamajor';
$handler->display->display_options['fields']['name']['empty'] = '<strong><em>none</em></strong>';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'program_para' => 'program_para',
);
/* Filter criterion: Content: Metamajor (field_area_of_interest) */
$handler->display->display_options['filters']['field_area_of_interest_tid']['id'] = 'field_area_of_interest_tid';
$handler->display->display_options['filters']['field_area_of_interest_tid']['table'] = 'field_data_field_area_of_interest';
$handler->display->display_options['filters']['field_area_of_interest_tid']['field'] = 'field_area_of_interest_tid';
$handler->display->display_options['filters']['field_area_of_interest_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_area_of_interest_tid']['expose']['operator_id'] = 'field_area_of_interest_tid_op';
$handler->display->display_options['filters']['field_area_of_interest_tid']['expose']['label'] = 'Area of interest';
$handler->display->display_options['filters']['field_area_of_interest_tid']['expose']['operator'] = 'field_area_of_interest_tid_op';
$handler->display->display_options['filters']['field_area_of_interest_tid']['expose']['identifier'] = 'metamajor';
$handler->display->display_options['filters']['field_area_of_interest_tid']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_area_of_interest_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_area_of_interest_tid']['vocabulary'] = 'meta_majors';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'explore-all-programs';
