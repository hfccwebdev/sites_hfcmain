<?php

$view = new view();
$view->name = 'news_promoted_sorting';
$view->description = 'Draggable Sorting for Promoted News items';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Promoted News Sorting';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Promoted News Sorting';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer nodes';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '40';
$handler->display->display_options['style_plugin'] = 'table';
/* Field: Draggableviews: Content */
$handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['table'] = 'node';
$handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['handler'] = 'draggableviews_handler_fieldapi';
$handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['draggableviews_handler_fieldapi'] = array(
  'field' => 'field_data_field_news_order_front:field_news_order_front_value',
);
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: Retire Date */
$handler->display->display_options['fields']['field_news_retire_date']['id'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['field'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: News Order Front */
$handler->display->display_options['fields']['field_news_order_front']['id'] = 'field_news_order_front';
$handler->display->display_options['fields']['field_news_order_front']['table'] = 'field_data_field_news_order_front';
$handler->display->display_options['fields']['field_news_order_front']['field'] = 'field_news_order_front';
$handler->display->display_options['fields']['field_news_order_front']['label'] = 'Sort Order';
$handler->display->display_options['fields']['field_news_order_front']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Sort criterion: Content: News Order Front (field_news_order_front) */
$handler->display->display_options['sorts']['field_news_order_front_value']['id'] = 'field_news_order_front_value';
$handler->display->display_options['sorts']['field_news_order_front_value']['table'] = 'field_data_field_news_order_front';
$handler->display->display_options['sorts']['field_news_order_front_value']['field'] = 'field_news_order_front_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['group'] = 1;
/* Filter criterion: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['filters']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['filters']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['value'] = array(
  'front' => 'front',
);
$handler->display->display_options['filters']['field_news_promote_value']['group'] = 1;

/* Display: Front News Sort */
$handler = $view->new_display('page', 'Front News Sort', 'page_news_front_sort');
$handler->display->display_options['path'] = 'admin/content/news/sort/front';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Front page news';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Promoted news sort';
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Public News Sort */
$handler = $view->new_display('page', 'Public News Sort', 'page_news_public_sort');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Draggableviews: Content */
$handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['table'] = 'node';
$handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['handler'] = 'draggableviews_handler_fieldapi';
$handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['draggableviews_handler_fieldapi'] = array(
  'field' => 'field_data_field_news_order_public:field_news_order_public_value',
);
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: Retire Date */
$handler->display->display_options['fields']['field_news_retire_date']['id'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['field'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: News Order Public */
$handler->display->display_options['fields']['field_news_order_public']['id'] = 'field_news_order_public';
$handler->display->display_options['fields']['field_news_order_public']['table'] = 'field_data_field_news_order_public';
$handler->display->display_options['fields']['field_news_order_public']['field'] = 'field_news_order_public';
$handler->display->display_options['fields']['field_news_order_public']['label'] = 'Sort Order';
$handler->display->display_options['fields']['field_news_order_public']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Public (field_news_order_public) */
$handler->display->display_options['sorts']['field_news_order_public_value']['id'] = 'field_news_order_public_value';
$handler->display->display_options['sorts']['field_news_order_public_value']['table'] = 'field_data_field_news_order_public';
$handler->display->display_options['sorts']['field_news_order_public_value']['field'] = 'field_news_order_public_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['group'] = 1;
/* Filter criterion: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['filters']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['filters']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['value'] = array(
  'public' => 'public',
);
$handler->display->display_options['filters']['field_news_promote_value']['group'] = 1;
$handler->display->display_options['path'] = 'admin/content/news/sort/public';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Public news';
$handler->display->display_options['menu']['weight'] = '2';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Student News Sort */
$handler = $view->new_display('page', 'Student News Sort', 'page_news_student_sort');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Draggableviews: Content */
$handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['table'] = 'node';
$handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['handler'] = 'draggableviews_handler_fieldapi';
$handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['draggableviews_handler_fieldapi'] = array(
  'field' => 'field_data_field_news_order_student:field_news_order_student_value',
);
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: Retire Date */
$handler->display->display_options['fields']['field_news_retire_date']['id'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['field'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: News Order Student */
$handler->display->display_options['fields']['field_news_order_student']['id'] = 'field_news_order_student';
$handler->display->display_options['fields']['field_news_order_student']['table'] = 'field_data_field_news_order_student';
$handler->display->display_options['fields']['field_news_order_student']['field'] = 'field_news_order_student';
$handler->display->display_options['fields']['field_news_order_student']['label'] = 'Sort Order';
$handler->display->display_options['fields']['field_news_order_student']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Student (field_news_order_student) */
$handler->display->display_options['sorts']['field_news_order_student_value']['id'] = 'field_news_order_student_value';
$handler->display->display_options['sorts']['field_news_order_student_value']['table'] = 'field_data_field_news_order_student';
$handler->display->display_options['sorts']['field_news_order_student_value']['field'] = 'field_news_order_student_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['group'] = 1;
/* Filter criterion: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['filters']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['filters']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['value'] = array(
  'student' => 'student',
);
$handler->display->display_options['filters']['field_news_promote_value']['group'] = 1;
$handler->display->display_options['path'] = 'admin/content/news/sort/student';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Student news';
$handler->display->display_options['menu']['weight'] = '4';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Employee News Sort */
$handler = $view->new_display('page', 'Employee News Sort', 'page_news_employee_sort');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Draggableviews: Content */
$handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['table'] = 'node';
$handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
$handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['handler'] = 'draggableviews_handler_fieldapi';
$handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
$handler->display->display_options['fields']['draggableviews']['draggableviews']['draggableviews_handler_fieldapi'] = array(
  'field' => 'field_data_field_news_order_employee:field_news_order_employee_value',
);
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: Retire Date */
$handler->display->display_options['fields']['field_news_retire_date']['id'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['field'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
/* Field: Content: News Order Employee */
$handler->display->display_options['fields']['field_news_order_employee']['id'] = 'field_news_order_employee';
$handler->display->display_options['fields']['field_news_order_employee']['table'] = 'field_data_field_news_order_employee';
$handler->display->display_options['fields']['field_news_order_employee']['field'] = 'field_news_order_employee';
$handler->display->display_options['fields']['field_news_order_employee']['label'] = 'Sort Order';
$handler->display->display_options['fields']['field_news_order_employee']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: News Order Employee (field_news_order_employee) */
$handler->display->display_options['sorts']['field_news_order_employee_value']['id'] = 'field_news_order_employee_value';
$handler->display->display_options['sorts']['field_news_order_employee_value']['table'] = 'field_data_field_news_order_employee';
$handler->display->display_options['sorts']['field_news_order_employee_value']['field'] = 'field_news_order_employee_value';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['group'] = 1;
/* Filter criterion: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['filters']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['filters']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['filters']['field_news_promote_value']['value'] = array(
  'employee' => 'employee',
);
$handler->display->display_options['filters']['field_news_promote_value']['group'] = 1;
$handler->display->display_options['path'] = 'admin/content/news/sort/employee';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Employee news';
$handler->display->display_options['menu']['weight'] = '6';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
