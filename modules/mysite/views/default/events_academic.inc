<?php

$view = new view();
$view->name = 'events_academic';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Events - Academic';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'News Archive';
$handler->display->display_options['css_class'] = 'upcoming-events-list';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'custom_url';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['bef'] = array(
  'general' => array(
    'input_required' => 0,
    'text_input_required' => array(
      'text_input_required' => array(
        'value' => 'Select any filter and click on Apply to see results',
        'format' => 'markdown',
      ),
    ),
    'allow_secondary' => 0,
    'secondary_label' => 'Advanced options',
    'secondary_collapse_override' => '0',
  ),
  'field_news_academic_term_value_1' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 1,
      'bef_collapsible' => 1,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
  'field_news_academic_term_value_2' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 1,
      'bef_collapsible' => 1,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
  'field_news_tags_tid_1' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 1,
      'bef_collapsible' => 1,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
          1 => 'vocabulary',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
);
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '60';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_news_event_date_2',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['row_class'] = 'event-card [field_important_event]';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'title' => 'title',
  'field_news_event_date' => 'field_news_event_date',
);
$handler->display->display_options['row_options']['separator'] = ' ';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = '';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_news_event_date']['element_wrapper_class'] = 'event-date';
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'short_calendar_with_time',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_news_event_date']['group_column'] = 'entity_id';
$handler->display->display_options['fields']['field_news_event_date']['group_rows'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_event_date']['delta_first_last'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['separator'] = ' – ';
/* Field: Content: Location */
$handler->display->display_options['fields']['field_news_event_location']['id'] = 'field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['table'] = 'field_data_field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['field'] = 'field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['label'] = '';
$handler->display->display_options['fields']['field_news_event_location']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_location']['element_label_colon'] = FALSE;
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date_1']['id'] = 'field_news_event_date_1';
$handler->display->display_options['fields']['field_news_event_date_1']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['label'] = '';
$handler->display->display_options['fields']['field_news_event_date_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date_1']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
$handler->display->display_options['fields']['field_news_event_date_1']['delta_offset'] = '0';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['text'] = '<h4>[title]</h4>
[field_news_event_location] [field_news_event_date_1] ';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['title']['element_wrapper_class'] = 'event-info';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
/* Sort criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['sorts']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['sorts']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['sorts']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  0 => '138',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
/* Filter criterion: Content: Event Date(s) - end date (field_news_event_date:value2) */
$handler->display->display_options['filters']['field_news_event_date_value2']['id'] = 'field_news_event_date_value2';
$handler->display->display_options['filters']['field_news_event_date_value2']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_date_value2']['field'] = 'field_news_event_date_value2';
$handler->display->display_options['filters']['field_news_event_date_value2']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_event_date_value2']['default_date'] = 'now';
/* Filter criterion: Content: Event Date(s) (field_news_event_date:delta) */
$handler->display->display_options['filters']['delta']['id'] = 'delta';
$handler->display->display_options['filters']['delta']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['delta']['field'] = 'delta';
$handler->display->display_options['filters']['delta']['operator'] = '<=';
$handler->display->display_options['filters']['delta']['value']['value'] = '0';
/* Filter criterion: Content: Academic Term (field_news_academic_term) */
$handler->display->display_options['filters']['field_news_academic_term_value']['id'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['filters']['field_news_academic_term_value']['field'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['value'] = array(
  '19/WI' => '19/WI',
  '19/SP' => '19/SP',
  '19/SU' => '19/SU',
);
$handler->display->display_options['filters']['field_news_academic_term_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['operator_id'] = 'field_news_academic_term_value_op';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['label'] = 'Current Year';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['operator'] = 'field_news_academic_term_value_op';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['identifier'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  17 => 0,
  3 => 0,
  4 => 0,
  7 => 0,
  14 => 0,
  15 => 0,
  18 => 0,
  19 => 0,
  20 => 0,
  21 => 0,
);
/* Filter criterion: Content: Academic Term (field_news_academic_term) */
$handler->display->display_options['filters']['field_news_academic_term_value_1']['id'] = 'field_news_academic_term_value_1';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['field'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['value'] = array(
  '19/FA' => '19/FA',
  '20/WI' => '20/WI',
  '20/SP' => '20/SP',
  '20/SU' => '20/SU',
  '20/FA' => '20/FA',
  '21/WI' => '21/WI',
  '21/SP' => '21/SP',
  '21/SU' => '21/SU',
  '21/FA' => '21/FA',
  '22/WI' => '22/WI',
  '22/SP' => '22/SP',
  '22/SU' => '22/SU',
  '22/FA' => '22/FA',
);
$handler->display->display_options['filters']['field_news_academic_term_value_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['operator_id'] = 'field_news_academic_term_value_1_op';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['label'] = 'Future Semesters';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['operator'] = 'field_news_academic_term_value_1_op';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['identifier'] = 'field_news_academic_term_value_1';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  17 => 0,
  3 => 0,
  4 => 0,
  7 => 0,
  14 => 0,
  15 => 0,
  18 => 0,
  19 => 0,
  20 => 0,
  21 => 0,
);

/* Display: Upcoming academic */
$handler = $view->new_display('page', 'Upcoming academic', 'page_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Upcoming Academic and Enrollment Dates';
$handler->display->display_options['defaults']['group_by'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
$handler->display->display_options['defaults']['footer'] = FALSE;
/* Footer: Global: Text area */
$handler->display->display_options['footer']['area']['id'] = 'area';
$handler->display->display_options['footer']['area']['table'] = 'views';
$handler->display->display_options['footer']['area']['field'] = 'area';
$handler->display->display_options['footer']['area']['label'] = 'disclaimer';
$handler->display->display_options['footer']['area']['content'] = '**Please note that dates are subject to change at the discretion of the College. Students should bookmark this page and check for updates. See details regarding [Academic and Attendance Policies](/registration-and-records/academic-calendar-info).**';
$handler->display->display_options['footer']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Academic Term */
$handler->display->display_options['fields']['field_news_academic_term']['id'] = 'field_news_academic_term';
$handler->display->display_options['fields']['field_news_academic_term']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['fields']['field_news_academic_term']['field'] = 'field_news_academic_term';
$handler->display->display_options['fields']['field_news_academic_term']['label'] = '';
$handler->display->display_options['fields']['field_news_academic_term']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_academic_term']['element_label_colon'] = FALSE;
/* Field: Content: Location */
$handler->display->display_options['fields']['field_news_event_location']['id'] = 'field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['table'] = 'field_data_field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['field'] = 'field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['label'] = '';
$handler->display->display_options['fields']['field_news_event_location']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_location']['element_label_colon'] = FALSE;
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date_1']['id'] = 'field_news_event_date_1';
$handler->display->display_options['fields']['field_news_event_date_1']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['label'] = '';
$handler->display->display_options['fields']['field_news_event_date_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date_1']['settings'] = array(
  'format_type' => 'long',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
  'show_repeat_rule' => 'show',
);
$handler->display->display_options['fields']['field_news_event_date_1']['delta_offset'] = '0';
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = '';
$handler->display->display_options['fields']['field_news_event_date']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['alter']['text'] = '<p>[field_news_event_date]</p>';
$handler->display->display_options['fields']['field_news_event_date']['element_label_type'] = 'strong';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['field_news_event_date']['element_wrapper_class'] = 'event-date';
$handler->display->display_options['fields']['field_news_event_date']['type'] = 'mysite_date_event_days';
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'long',
  'custom_date_format' => '',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
$handler->display->display_options['fields']['field_news_event_date']['group_column'] = 'entity_id';
$handler->display->display_options['fields']['field_news_event_date']['group_rows'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_event_date']['delta_first_last'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['separator'] = ' – ';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['exclude'] = TRUE;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['text'] = '<div class="event-info"><h4>[title]</h4>
[field_news_event_date_1] <br>
[body]
</div>';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_wrapper_type'] = 'span';
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
$handler->display->display_options['fields']['edit_node']['hide_empty'] = TRUE;
/* Field: Content: important_event */
$handler->display->display_options['fields']['field_important_event']['id'] = 'field_important_event';
$handler->display->display_options['fields']['field_important_event']['table'] = 'field_data_field_important_event';
$handler->display->display_options['fields']['field_important_event']['field'] = 'field_important_event';
$handler->display->display_options['fields']['field_important_event']['label'] = '';
$handler->display->display_options['fields']['field_important_event']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_important_event']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_important_event']['alter']['text'] = 'important-event';
$handler->display->display_options['fields']['field_important_event']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_important_event']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['field_important_event']['delta_offset'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Event Date(s) - end date (field_news_event_date:value2) */
$handler->display->display_options['filters']['field_news_event_date_value2']['id'] = 'field_news_event_date_value2';
$handler->display->display_options['filters']['field_news_event_date_value2']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_date_value2']['field'] = 'field_news_event_date_value2';
$handler->display->display_options['filters']['field_news_event_date_value2']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_event_date_value2']['group'] = 1;
$handler->display->display_options['filters']['field_news_event_date_value2']['default_date'] = 'now';
/* Filter criterion: Content: Academic Term (field_news_academic_term) */
$handler->display->display_options['filters']['field_news_academic_term_value']['id'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['filters']['field_news_academic_term_value']['field'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['operator'] = 'not empty';
$handler->display->display_options['filters']['field_news_academic_term_value']['group'] = 1;
/* Filter criterion: Content: Academic Term (field_news_academic_term) */
$handler->display->display_options['filters']['field_news_academic_term_value_1']['id'] = 'field_news_academic_term_value_1';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['field'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['value'] = array(
  '24/WI' => '24/WI',
  '24/SU' => '24/SU',
  '24/FA' => '24/FA',
);
$handler->display->display_options['filters']['field_news_academic_term_value_1']['group'] = 1;
$handler->display->display_options['filters']['field_news_academic_term_value_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['operator_id'] = 'field_news_academic_term_value_1_op';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['label'] = 'Current Year';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['operator'] = 'field_news_academic_term_value_1_op';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['identifier'] = 'field_news_academic_term_value_1';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_academic_term_value_1']['expose']['reduce'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value_1']['group_info']['label'] = 'Academic Term (field_news_academic_term)';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['group_info']['identifier'] = 'field_news_academic_term_value_1';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['group_info']['default_group'] = '1';
$handler->display->display_options['filters']['field_news_academic_term_value_1']['group_info']['group_items'] = array(
  1 => array(
    'title' => 'Current Year',
    'operator' => 'or',
    'value' => array(
      '24/WI' => '24/WI',
      '24/SU' => '24/SU',
      '24/FA' => '24/FA',
    ),
  ),
  2 => array(
    'title' => 'Future Semesters',
    'operator' => 'or',
    'value' => array(
      '25/WI' => '25/WI',
      '25/SU' => '25/SU',
      '25/FA' => '25/FA',
    ),
  ),
);
/* Filter criterion: Content: Academic Term (field_news_academic_term) */
$handler->display->display_options['filters']['field_news_academic_term_value_2']['id'] = 'field_news_academic_term_value_2';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['field'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['value'] = array(
  '25/WI' => '25/WI',
  '25/SU' => '25/SU',
  '25/FA' => '25/FA',
);
$handler->display->display_options['filters']['field_news_academic_term_value_2']['group'] = 1;
$handler->display->display_options['filters']['field_news_academic_term_value_2']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['operator_id'] = 'field_news_academic_term_value_2_op';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['label'] = 'Future Semesters';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['operator'] = 'field_news_academic_term_value_2_op';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['identifier'] = 'field_news_academic_term_value_2';
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_academic_term_value_2']['expose']['reduce'] = TRUE;
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid_1']['id'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid_1']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid_1']['operator'] = 'and';
$handler->display->display_options['filters']['field_news_tags_tid_1']['value'] = array(
  116 => '116',
  117 => '117',
  226 => '226',
  118 => '118',
  205 => '205',
  119 => '119',
  120 => '120',
  121 => '121',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['group'] = 1;
$handler->display->display_options['filters']['field_news_tags_tid_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator_id'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['label'] = 'Course Categories';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['identifier'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['reduce'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid_1']['type'] = 'select';
$handler->display->display_options['filters']['field_news_tags_tid_1']['vocabulary'] = 'tags';
$handler->display->display_options['path'] = 'events/academic/upcoming';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Academic Events';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Academic Events';
$handler->display->display_options['tab_options']['weight'] = '0';
