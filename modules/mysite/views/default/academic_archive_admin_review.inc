<?php

$view = new view();
$view->name = 'academic_archive_admin_review';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Academic Archive Admin Review';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'News Archive';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'custom_url';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer nodes';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['bef'] = array(
  'general' => array(
    'input_required' => 0,
    'text_input_required' => array(
      'text_input_required' => array(
        'value' => 'Select any filter and click on Apply to see results',
        'format' => 'markdown',
      ),
    ),
    'allow_secondary' => 0,
    'secondary_label' => 'Advanced options',
    'secondary_collapse_override' => '0',
  ),
  'field_news_academic_term_value' => array(
    'bef_format' => 'default',
    'more_options' => array(
      'bef_select_all_none' => FALSE,
      'bef_collapsible' => 0,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
  'field_news_tags_tid' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => FALSE,
      'bef_collapsible' => 0,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
          1 => 'vocabulary',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
);
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '40';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_news_event_date' => 'field_news_event_date',
  'body' => 'body',
  'field_important_event' => 'field_important_event',
  'field_news_tags' => 'field_news_tags',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_news_event_date' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'body' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_important_event' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_news_tags' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = '';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'short',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
/* Sort criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['sorts']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['sorts']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['sorts']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
/* Filter criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['filters']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['filters']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
$handler->display->display_options['filters']['field_news_event_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_event_date_value']['default_date'] = 'now';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['operator'] = 'not';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  0 => '113',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
/* Filter criterion: Broken/missing handler */
$handler->display->display_options['filters']['private']['id'] = 'private';
$handler->display->display_options['filters']['private']['table'] = 'private';
$handler->display->display_options['filters']['private']['field'] = 'private';
$handler->display->display_options['filters']['private']['value'] = '0';

/* Display: AC Archive */
$handler = $view->new_display('page', 'AC Archive', 'page_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Archive: Academic and Enrollment Dates ';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Event';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = 'Date';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'Notes';
$handler->display->display_options['fields']['body']['type'] = 'text_plain';
/* Field: Content: important_event */
$handler->display->display_options['fields']['field_important_event']['id'] = 'field_important_event';
$handler->display->display_options['fields']['field_important_event']['table'] = 'field_data_field_important_event';
$handler->display->display_options['fields']['field_important_event']['field'] = 'field_important_event';
$handler->display->display_options['fields']['field_important_event']['label'] = 'Important Event';
$handler->display->display_options['fields']['field_important_event']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_important_event']['alter']['text'] = 'Y';
$handler->display->display_options['fields']['field_important_event']['delta_offset'] = '0';
/* Field: Field: Tags */
$handler->display->display_options['fields']['field_news_tags']['id'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['field'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['type'] = 'taxonomy_term_reference_plain';
$handler->display->display_options['fields']['field_news_tags']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_tags']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_news_tags']['separator'] = '<br>';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Edit link';
$handler->display->display_options['fields']['edit_node']['hide_empty'] = TRUE;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['sorts']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['sorts']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['sorts']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
/* Filter criterion: Content: Academic Term (field_news_academic_term) */
$handler->display->display_options['filters']['field_news_academic_term_value']['id'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['table'] = 'field_data_field_news_academic_term';
$handler->display->display_options['filters']['field_news_academic_term_value']['field'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['value'] = array(
  '18/FA' => '18/FA',
);
$handler->display->display_options['filters']['field_news_academic_term_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['operator_id'] = 'field_news_academic_term_value_op';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['label'] = 'Semester';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['operator'] = 'field_news_academic_term_value_op';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['identifier'] = 'field_news_academic_term_value';
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['field_news_academic_term_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  17 => 0,
  3 => 0,
  4 => 0,
  7 => 0,
  14 => 0,
  15 => 0,
  18 => 0,
  19 => 0,
  20 => 0,
  21 => 0,
);
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  113 => '113',
  114 => '114',
);
$handler->display->display_options['filters']['field_news_tags_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator_id'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['label'] = 'Calendar Type';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['identifier'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['required'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  9 => 0,
  11 => 0,
  10 => 0,
  8 => 0,
  13 => 0,
  12 => 0,
  7 => 0,
  6 => 0,
  14 => 0,
  15 => 0,
  16 => 0,
  17 => 0,
);
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['reduce'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['reduce_duplicates'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
$handler->display->display_options['path'] = 'admin/content/news/academic-archive';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Academic Archive';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
