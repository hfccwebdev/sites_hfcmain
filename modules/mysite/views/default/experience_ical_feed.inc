<?php

$view = new view();
$view->name = 'experience_ical_feed';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Experience iCal Feed';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Article/Event Short Title */
$handler->display->display_options['fields']['field_news_event_short_title']['id'] = 'field_news_event_short_title';
$handler->display->display_options['fields']['field_news_event_short_title']['table'] = 'field_data_field_news_event_short_title';
$handler->display->display_options['fields']['field_news_event_short_title']['field'] = 'field_news_event_short_title';
$handler->display->display_options['fields']['field_news_event_short_title']['label'] = '';
$handler->display->display_options['fields']['field_news_event_short_title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_short_title']['empty'] = '[title]';
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = '';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'long',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['exclude'] = TRUE;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
$handler->display->display_options['fields']['body']['settings'] = array(
  'trim_length' => '400',
);
/* Field: Content: News Summary */
$handler->display->display_options['fields']['field_news_cut_line']['id'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['table'] = 'field_data_field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['field'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['empty'] = '[body]';
/* Field: Content: Location */
$handler->display->display_options['fields']['field_news_event_location']['id'] = 'field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['table'] = 'field_data_field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['field'] = 'field_news_event_location';
$handler->display->display_options['fields']['field_news_event_location']['label'] = '';
$handler->display->display_options['fields']['field_news_event_location']['element_label_colon'] = FALSE;
/* Sort criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['sorts']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['sorts']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['sorts']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
/* Filter criterion: Content: Event Date(s) - end date (field_news_event_date:value2) */
$handler->display->display_options['filters']['field_news_event_date_value2']['id'] = 'field_news_event_date_value2';
$handler->display->display_options['filters']['field_news_event_date_value2']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_date_value2']['field'] = 'field_news_event_date_value2';
$handler->display->display_options['filters']['field_news_event_date_value2']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_event_date_value2']['default_date'] = 'now';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  0 => '138',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';

/* Display: Feed */
$handler = $view->new_display('feed', 'Feed', 'feed_1');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '0';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'date_ical';
$handler->display->display_options['style_options']['cal_name'] = '';
$handler->display->display_options['row_plugin'] = 'date_ical_fields';
$handler->display->display_options['row_options']['date_field'] = 'field_news_event_date';
$handler->display->display_options['row_options']['title_field'] = 'field_news_event_short_title';
$handler->display->display_options['row_options']['description_field'] = 'field_news_cut_line';
$handler->display->display_options['row_options']['location_field'] = 'field_news_event_location';
$handler->display->display_options['row_options']['additional_settings'] = array(
  'skip_blank_dates' => 1,
);
$handler->display->display_options['path'] = 'events.ics';
