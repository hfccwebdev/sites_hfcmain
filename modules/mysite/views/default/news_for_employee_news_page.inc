<?php

$view = new view();
$view->name = 'news_for_employee_news_page';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'News for Employee News page';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['css_class'] = 'highlight-header-wide news-cards-grid';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'custom_url';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['bef'] = array(
  'general' => array(
    'input_required' => 0,
    'text_input_required' => array(
      'text_input_required' => array(
        'value' => 'Select any filter and click on Apply to see results',
        'format' => 'markdown',
      ),
    ),
    'allow_secondary' => 0,
    'secondary_label' => 'Advanced options',
    'secondary_collapse_override' => '0',
  ),
  'field_news_tags_tid_1' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 0,
      'bef_collapsible' => 0,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
          1 => 'vocabulary',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
);
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'newsgrid';
/* Header: Global: Text area */
$handler->display->display_options['header']['area_1']['id'] = 'area_1';
$handler->display->display_options['header']['area_1']['table'] = 'views';
$handler->display->display_options['header']['area_1']['field'] = 'area';
/* Sort criterion: Content: Retire Date (field_news_retire_date) */
$handler->display->display_options['sorts']['field_news_retire_date_value']['id'] = 'field_news_retire_date_value';
$handler->display->display_options['sorts']['field_news_retire_date_value']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['sorts']['field_news_retire_date_value']['field'] = 'field_news_retire_date_value';
/* Sort criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
/* Contextual filter: Content: Tags (field_news_tags) */
$handler->display->display_options['arguments']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['arguments']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['arguments']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['arguments']['field_news_tags_tid']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_tags_tid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_tags_tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_tags_tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_tags_tid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Retire Date Timestamp */
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['id'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['field'] = 'field_news_retire_ending_timestamp';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_retire_ending_timestamp']['group'] = 1;

/* Display: campus events - this week */
$handler = $view->new_display('block', 'campus events - this week', 'block_7');
$handler->display->display_options['defaults']['link_display'] = FALSE;
$handler->display->display_options['link_display'] = 'page_1';
$handler->display->display_options['link_url'] = '/news-new';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '##Campus Events This Week';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['sorts']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['sorts']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['sorts']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'evtemployee';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['filters']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['filters']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
$handler->display->display_options['filters']['field_news_event_date_value']['operator'] = 'between';
$handler->display->display_options['filters']['field_news_event_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_event_date_value']['default_date'] = 'monday this week';
$handler->display->display_options['filters']['field_news_event_date_value']['default_to_date'] = 'now +7 days';
/* Filter criterion: Content: Event End Timestamp */
$handler->display->display_options['filters']['field_news_event_ending_timestamp']['id'] = 'field_news_event_ending_timestamp';
$handler->display->display_options['filters']['field_news_event_ending_timestamp']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_ending_timestamp']['field'] = 'field_news_event_ending_timestamp';
$handler->display->display_options['filters']['field_news_event_ending_timestamp']['operator'] = '>=';
$handler->display->display_options['filters']['field_news_event_ending_timestamp']['group'] = 1;
/* Filter criterion: Content: Event Date(s) (field_news_event_date:delta) */
$handler->display->display_options['filters']['delta']['id'] = 'delta';
$handler->display->display_options['filters']['delta']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['delta']['field'] = 'delta';
$handler->display->display_options['filters']['delta']['operator'] = '<=';
$handler->display->display_options['filters']['delta']['value']['value'] = '0';
$handler->display->display_options['filters']['delta']['group'] = 1;
$handler->display->display_options['block_description'] = 'news 4 cols  - employee news page campus events this week';

/* Display: campus events - upcoming */
$handler = $view->new_display('block', 'campus events - upcoming', 'block_2');
$handler->display->display_options['defaults']['link_display'] = FALSE;
$handler->display->display_options['link_display'] = 'page_1';
$handler->display->display_options['link_url'] = '/news-new';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '## Campus Events Coming Soon';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['sorts']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['sorts']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['sorts']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'evtemployee';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Content: Event Date(s) -  start date (field_news_event_date) */
$handler->display->display_options['filters']['field_news_event_date_value']['id'] = 'field_news_event_date_value';
$handler->display->display_options['filters']['field_news_event_date_value']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['field_news_event_date_value']['field'] = 'field_news_event_date_value';
$handler->display->display_options['filters']['field_news_event_date_value']['operator'] = '>';
$handler->display->display_options['filters']['field_news_event_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_event_date_value']['default_date'] = 'now +7 days';
/* Filter criterion: Content: Event Date(s) (field_news_event_date:delta) */
$handler->display->display_options['filters']['delta']['id'] = 'delta';
$handler->display->display_options['filters']['delta']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['filters']['delta']['field'] = 'delta';
$handler->display->display_options['filters']['delta']['operator'] = '<=';
$handler->display->display_options['filters']['delta']['value']['value'] = '0';
$handler->display->display_options['block_description'] = 'news 4 cols  - employee news page campus events';

/* Display: save the date */
$handler = $view->new_display('block', 'save the date', 'block_3');
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['css_class'] = 'highlight-header-wide';
$handler->display->display_options['defaults']['link_display'] = FALSE;
$handler->display->display_options['link_display'] = 'page_1';
$handler->display->display_options['link_url'] = '/news-new';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '## Save the Date';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'evtsave';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'news 4 cols  - employee news page save the date';

/* Display: faculty */
$handler = $view->new_display('block', 'faculty', 'block_4');
$handler->display->display_options['defaults']['link_display'] = FALSE;
$handler->display->display_options['link_display'] = 'page_1';
$handler->display->display_options['link_url'] = '/news-new';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '##For Faculty and Instructors';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'faculty';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'news 4 cols  - employee news page faculty';

/* Display: join in */
$handler = $view->new_display('block', 'join in', 'block_5');
$handler->display->display_options['defaults']['link_display'] = FALSE;
$handler->display->display_options['link_display'] = 'page_1';
$handler->display->display_options['link_url'] = '/news-new';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = '## Join In: Help your HFC Community Today';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Promote to Landing Pages (field_news_promote) */
$handler->display->display_options['arguments']['field_news_promote_value']['id'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['table'] = 'field_data_field_news_promote';
$handler->display->display_options['arguments']['field_news_promote_value']['field'] = 'field_news_promote_value';
$handler->display->display_options['arguments']['field_news_promote_value']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_news_promote_value']['default_argument_options']['argument'] = 'evtjoin';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_news_promote_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_news_promote_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_news_promote_value']['limit'] = '0';
$handler->display->display_options['block_description'] = 'news 4 cols  - employee news page join in';
