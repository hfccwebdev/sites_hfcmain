<?php

$view = new view();
$view->name = 'meta_major';
$view->description = 'Overview list of Metamajors';
$view->tag = 'default';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'meta_major';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Programs of Study';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['style_options']['columns'] = '3';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
/* Field: Taxonomy term: Term ID */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
$handler->display->display_options['fields']['tid']['label'] = '';
$handler->display->display_options['fields']['tid']['exclude'] = TRUE;
$handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['tid']['separator'] = '';
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name_1']['id'] = 'name_1';
$handler->display->display_options['fields']['name_1']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name_1']['field'] = 'name';
$handler->display->display_options['fields']['name_1']['label'] = '';
$handler->display->display_options['fields']['name_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['name_1']['alter']['path'] = 'meta-major/[tid]';
$handler->display->display_options['fields']['name_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['name_1']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['name_1']['element_wrapper_class'] = 'highlight-link, no-link';
$handler->display->display_options['fields']['name_1']['element_default_classes'] = FALSE;
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['exclude'] = TRUE;
$handler->display->display_options['fields']['name']['alter']['text'] = '<h3 class="menu-title">[]</h3>';
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['name']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['name']['element_wrapper_class'] = 'highlight-title, no-link';
$handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
/* Field: Taxonomy term: Term description */
$handler->display->display_options['fields']['description']['id'] = 'description';
$handler->display->display_options['fields']['description']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['description']['field'] = 'description';
$handler->display->display_options['fields']['description']['label'] = '';
$handler->display->display_options['fields']['description']['exclude'] = TRUE;
$handler->display->display_options['fields']['description']['alter']['max_length'] = '400';
$handler->display->display_options['fields']['description']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
/* Field: Taxonomy term: SVG Photo */
$handler->display->display_options['fields']['field_svg_photo']['id'] = 'field_svg_photo';
$handler->display->display_options['fields']['field_svg_photo']['table'] = 'field_data_field_svg_photo';
$handler->display->display_options['fields']['field_svg_photo']['field'] = 'field_svg_photo';
$handler->display->display_options['fields']['field_svg_photo']['label'] = '';
$handler->display->display_options['fields']['field_svg_photo']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_svg_photo']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_svg_photo']['alter']['text'] = '<img src="[field_svg_photo]" alt="[name]">';
$handler->display->display_options['fields']['field_svg_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_svg_photo']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_svg_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_svg_photo']['type'] = 'file_url_plain';
$handler->display->display_options['fields']['field_svg_photo']['settings'] = array(
  'text' => 'Download [file:name]',
);
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="highlight-link no-link"><span><a href="/meta-major/[tid]">[name]</a></div>
[field_svg_photo]
<div class="highlight-title">[name]</div>';
$handler->display->display_options['fields']['nothing']['element_type'] = '0';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
/* Filter criterion: Taxonomy term: Vocabulary */
$handler->display->display_options['filters']['vid']['id'] = 'vid';
$handler->display->display_options['filters']['vid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['filters']['vid']['field'] = 'vid';
$handler->display->display_options['filters']['vid']['value'] = array(
  4 => '4',
);
/* Filter criterion: Taxonomy term: Inactive (field_inactive) */
$handler->display->display_options['filters']['field_inactive_value']['id'] = 'field_inactive_value';
$handler->display->display_options['filters']['field_inactive_value']['table'] = 'field_data_field_inactive';
$handler->display->display_options['filters']['field_inactive_value']['field'] = 'field_inactive_value';
$handler->display->display_options['filters']['field_inactive_value']['operator'] = 'not';
$handler->display->display_options['filters']['field_inactive_value']['value'] = array(
  1 => '1',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['metatags'] = array(
  'und' => array(
    'title' => array(
      'value' => '[view:title] | [site:name]',
      'default' => '[view:title] | [site:name]',
    ),
    'description' => array(
      'value' => 'Browse HFC\'s programs grouped by meta-majors.',
      'default' => '[view:description]',
    ),
    'abstract' => array(
      'value' => '',
      'default' => '',
    ),
    'keywords' => array(
      'value' => '',
      'default' => '',
    ),
    'robots' => array(
      'value' => array(
        'index' => 0,
        'follow' => 0,
        'noindex' => 0,
        'nofollow' => 0,
        'noarchive' => 0,
        'nosnippet' => 0,
        'noodp' => 0,
        'noydir' => 0,
        'noimageindex' => 0,
        'notranslate' => 0,
      ),
      'default' => array(
        'index' => 0,
        'follow' => 0,
        'noindex' => 0,
        'nofollow' => 0,
        'noarchive' => 0,
        'nosnippet' => 0,
        'noodp' => 0,
        'noydir' => 0,
        'noimageindex' => 0,
        'notranslate' => 0,
      ),
    ),
    'news_keywords' => array(
      'value' => '',
      'default' => '',
    ),
    'standout' => array(
      'value' => '',
      'default' => '',
    ),
    'rating' => array(
      'value' => '',
      'default' => '',
    ),
    'referrer' => array(
      'value' => '',
      'default' => '',
    ),
    'rights' => array(
      'value' => 'Henry Ford College',
      'default' => 'Henry Ford College',
    ),
    'image_src' => array(
      'value' => '',
      'default' => '',
    ),
    'canonical' => array(
      'value' => '[view:url]',
      'default' => '[view:url]',
    ),
    'shortlink' => array(
      'value' => '[current-page:url:unaliased]',
      'default' => '[current-page:url:unaliased]',
    ),
    'publisher' => array(
      'value' => '',
      'default' => '',
    ),
    'author' => array(
      'value' => '',
      'default' => '',
    ),
    'original-source' => array(
      'value' => '',
      'default' => '',
    ),
    'prev' => array(
      'value' => '',
      'default' => '',
    ),
    'next' => array(
      'value' => '',
      'default' => '',
    ),
    'revisit-after' => array(
      'value' => '',
      'period' => '',
      'default' => '',
    ),
    'content-language' => array(
      'value' => '',
      'default' => '',
    ),
    'geo.position' => array(
      'value' => '',
      'default' => '',
    ),
    'geo.placename' => array(
      'value' => '',
      'default' => '',
    ),
    'geo.region' => array(
      'value' => '',
      'default' => '',
    ),
    'icbm' => array(
      'value' => '',
      'default' => '',
    ),
    'refresh' => array(
      'value' => '',
      'default' => '',
    ),
    'fb:admins' => array(
      'value' => '',
      'default' => '',
    ),
    'fb:app_id' => array(
      'value' => '',
      'default' => '',
    ),
    'og:type' => array(
      'value' => 'article',
      'default' => 'article',
    ),
    'og:url' => array(
      'value' => '[current-page:url:absolute]',
      'default' => '[current-page:url:absolute]',
    ),
    'og:title' => array(
      'value' => '[current-page:title]',
      'default' => '[current-page:title]',
    ),
    'og:determiner' => array(
      'value' => '',
      'default' => '',
    ),
    'og:description' => array(
      'value' => '',
      'default' => '',
    ),
    'og:updated_time' => array(
      'value' => '',
      'default' => '',
    ),
    'og:see_also' => array(
      'value' => '',
      'default' => '',
    ),
    'og:image' => array(
      'value' => '',
      'default' => '',
    ),
    'og:image:url' => array(
      'value' => '',
      'default' => '',
    ),
    'og:image:secure_url' => array(
      'value' => '',
      'default' => '',
    ),
    'og:image:type' => array(
      'value' => '',
      'default' => '',
    ),
    'og:image:width' => array(
      'value' => '',
      'default' => '',
    ),
    'og:image:height' => array(
      'value' => '',
      'default' => '',
    ),
    'og:latitude' => array(
      'value' => '42.324216',
      'default' => '42.324216',
    ),
    'og:longitude' => array(
      'value' => '-83.237181',
      'default' => '-83.237181',
    ),
    'og:street_address' => array(
      'value' => '5101 Evergreen',
      'default' => '5101 Evergreen',
    ),
    'og:locality' => array(
      'value' => 'Dearborn',
      'default' => 'Dearborn',
    ),
    'og:region' => array(
      'value' => 'Michigan',
      'default' => 'Michigan',
    ),
    'og:postal_code' => array(
      'value' => '48128',
      'default' => '48128',
    ),
    'og:country_name' => array(
      'value' => 'United States',
      'default' => 'United States',
    ),
    'og:email' => array(
      'value' => 'webmaster@hfcc.edu',
      'default' => 'webmaster@hfcc.edu',
    ),
    'og:phone_number' => array(
      'value' => '800-585-HFCC',
      'default' => '800-585-HFCC',
    ),
    'og:fax_number' => array(
      'value' => '',
      'default' => '',
    ),
    'og:locale' => array(
      'value' => '',
      'default' => '',
    ),
    'og:locale:alternate' => array(
      'value' => '',
      'default' => '',
    ),
    'article:author' => array(
      'value' => '',
      'default' => '',
    ),
    'article:publisher' => array(
      'value' => '',
      'default' => '',
    ),
    'article:section' => array(
      'value' => '',
      'default' => '',
    ),
    'article:tag' => array(
      'value' => '',
      'default' => '',
    ),
    'article:published_time' => array(
      'value' => '',
      'default' => '',
    ),
    'article:modified_time' => array(
      'value' => '',
      'default' => '',
    ),
    'article:expiration_time' => array(
      'value' => '',
      'default' => '',
    ),
    'profile:first_name' => array(
      'value' => '',
      'default' => '',
    ),
    'profile:last_name' => array(
      'value' => '',
      'default' => '',
    ),
    'profile:username' => array(
      'value' => '',
      'default' => '',
    ),
    'profile:gender' => array(
      'value' => '',
      'default' => '',
    ),
    'og:audio' => array(
      'value' => '',
      'default' => '',
    ),
    'og:audio:secure_url' => array(
      'value' => '',
      'default' => '',
    ),
    'og:audio:type' => array(
      'value' => '',
      'default' => '',
    ),
    'book:author' => array(
      'value' => '',
      'default' => '',
    ),
    'book:isbn' => array(
      'value' => '',
      'default' => '',
    ),
    'book:release_date' => array(
      'value' => '',
      'default' => '',
    ),
    'book:tag' => array(
      'value' => '',
      'default' => '',
    ),
    'og:video' => array(
      'value' => '',
      'default' => '',
    ),
    'og:video:secure_url' => array(
      'value' => '',
      'default' => '',
    ),
    'og:video:width' => array(
      'value' => '',
      'default' => '',
    ),
    'og:video:height' => array(
      'value' => '',
      'default' => '',
    ),
    'og:video:type' => array(
      'value' => '',
      'default' => '',
    ),
    'video:actor' => array(
      'value' => '',
      'default' => '',
    ),
    'video:actor:role' => array(
      'value' => '',
      'default' => '',
    ),
    'video:director' => array(
      'value' => '',
      'default' => '',
    ),
    'video:writer' => array(
      'value' => '',
      'default' => '',
    ),
    'video:duration' => array(
      'value' => '',
      'default' => '',
    ),
    'video:release_date' => array(
      'value' => '',
      'default' => '',
    ),
    'video:tag' => array(
      'value' => '',
      'default' => '',
    ),
    'video:series' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:card' => array(
      'value' => 'summary',
      'default' => 'summary',
    ),
    'twitter:creator' => array(
      'value' => '@hfcc',
      'default' => '@hfcc',
    ),
    'twitter:creator:id' => array(
      'value' => '21886888',
      'default' => '21886888',
    ),
    'twitter:url' => array(
      'value' => '[current-page:url:absolute]',
      'default' => '[current-page:url:absolute]',
    ),
    'twitter:title' => array(
      'value' => '[site:name]',
      'default' => '[site:name]',
    ),
    'twitter:description' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image:src' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image:width' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image:height' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image0' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image1' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image2' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:image3' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:player' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:player:width' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:player:height' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:player:stream' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:player:stream:content_type' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:country' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:name:iphone' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:id:iphone' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:url:iphone' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:name:ipad' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:id:ipad' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:url:ipad' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:name:googleplay' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:id:googleplay' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:app:url:googleplay' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:label1' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:data1' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:label2' => array(
      'value' => '',
      'default' => '',
    ),
    'twitter:data2' => array(
      'value' => '',
      'default' => '',
    ),
  ),
);
$handler->display->display_options['path'] = 'academics/programs';
