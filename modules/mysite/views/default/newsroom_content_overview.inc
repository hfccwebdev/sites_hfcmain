<?php

$view = new view();
$view->name = 'newsroom_content_overview';
$view->description = 'Provides an administrative overview of recently-edited newsroom content.';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Newsroom Content Overview';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Newsroom Content Overview';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  4 => '4',
  7 => '7',
  18 => '18',
  6 => '6',
  14 => '14',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'status' => 'status',
  'name' => 'name',
  'field_news_release_date' => 'field_news_release_date',
  'field_news_retire_date' => 'field_news_release_date',
  'field_news_event_date' => 'field_news_event_date',
  'field_news_tags' => 'field_news_tags',
  'created' => 'created',
  'changed' => 'changed',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['class'] = '';
$handler->display->display_options['style_options']['default'] = 'changed';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_news_release_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '<br>',
    'empty_column' => 0,
  ),
  'field_news_retire_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_news_event_date' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_news_tags' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Relationship: Content: Content author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
$handler->display->display_options['relationships']['uid']['label'] = 'Author';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Authored by';
$handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
/* Field: Field: Release Date */
$handler->display->display_options['fields']['field_news_release_date']['id'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['field'] = 'field_news_release_date';
$handler->display->display_options['fields']['field_news_release_date']['label'] = 'Release/Retire Dates';
$handler->display->display_options['fields']['field_news_release_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
/* Field: Field: Retire Date */
$handler->display->display_options['fields']['field_news_retire_date']['id'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['table'] = 'field_data_field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['field'] = 'field_news_retire_date';
$handler->display->display_options['fields']['field_news_retire_date']['settings'] = array(
  'format_type' => 'short_date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'show',
);
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
/* Field: Field: Tags */
$handler->display->display_options['fields']['field_news_tags']['id'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['field'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['delta_offset'] = '0';
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'short_date_only';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
/* Sort criterion: Content: Updated date */
$handler->display->display_options['sorts']['changed']['id'] = 'changed';
$handler->display->display_options['sorts']['changed']['table'] = 'node';
$handler->display->display_options['sorts']['changed']['field'] = 'changed';
$handler->display->display_options['sorts']['changed']['order'] = 'DESC';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Search: Search Terms */
$handler->display->display_options['filters']['keys']['id'] = 'keys';
$handler->display->display_options['filters']['keys']['table'] = 'search_index';
$handler->display->display_options['filters']['keys']['field'] = 'keys';
$handler->display->display_options['filters']['keys']['group'] = 1;
$handler->display->display_options['filters']['keys']['exposed'] = TRUE;
$handler->display->display_options['filters']['keys']['expose']['operator_id'] = 'keys_op';
$handler->display->display_options['filters']['keys']['expose']['label'] = 'Search';
$handler->display->display_options['filters']['keys']['expose']['operator'] = 'keys_op';
$handler->display->display_options['filters']['keys']['expose']['identifier'] = 'search';
$handler->display->display_options['filters']['keys']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['keys']['expose']['remember_roles'] = array(
  2 => '2',
);
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = '';
$handler->display->display_options['filters']['field_news_tags_tid']['group'] = 1;
$handler->display->display_options['filters']['field_news_tags_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator_id'] = 'tags_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['label'] = 'Tags';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['identifier'] = 'tags';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid_1']['id'] = 'field_news_tags_tid_1';
$handler->display->display_options['filters']['field_news_tags_tid_1']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid_1']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid_1']['operator'] = 'not';
$handler->display->display_options['filters']['field_news_tags_tid_1']['value'] = '';
$handler->display->display_options['filters']['field_news_tags_tid_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator_id'] = 'tags2_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['label'] = 'Tags';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['operator'] = 'field_news_tags_tid_1_op';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['identifier'] = 'tags2';
$handler->display->display_options['filters']['field_news_tags_tid_1']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid_1']['vocabulary'] = 'tags';

/* Display: Overview Page */
$handler = $view->new_display('page', 'Overview Page', 'page');
$handler->display->display_options['path'] = 'admin/content/news/overview';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Overview';
$handler->display->display_options['menu']['weight'] = '-90';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'tab';
$handler->display->display_options['tab_options']['title'] = 'News and Events';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'management';

/* Display: Tag Operations Page */
$handler = $view->new_display('page', 'Tag Operations Page', 'page_tags');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Authored by';
$handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
/* Field: Bulk operations: Content */
$handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
  'action::views_bulk_operations_modify_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'skip_permission_check' => 0,
    'override_label' => 0,
    'label' => '',
    'settings' => array(
      'show_all_tokens' => 1,
      'display_values' => array(
        'news::field_news_tags' => 'news::field_news_tags',
      ),
    ),
  ),
);
/* Field: Field: Tags */
$handler->display->display_options['fields']['field_news_tags']['id'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['field'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['delta_offset'] = '0';
$handler->display->display_options['path'] = 'admin/content/news/operations';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Bulk Operations';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
