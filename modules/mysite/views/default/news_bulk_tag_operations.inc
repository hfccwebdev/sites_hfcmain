<?php

$view = new view();
$view->name = 'news_bulk_tag_operations';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'News Bulk Tag Operations';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'News Bulk Tag Operations';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '25';
$handler->display->display_options['style_plugin'] = 'table';
/* Field: Bulk operations: Content */
$handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['label'] = 'Operations';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
  'action::views_bulk_operations_modify_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'skip_permission_check' => 0,
    'override_label' => 0,
    'label' => '',
    'settings' => array(
      'show_all_tokens' => 0,
      'display_values' => array(
        'news::field_news_tags' => 'news::field_news_tags',
      ),
    ),
  ),
);
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => 'medium',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: News Summary */
$handler->display->display_options['fields']['field_news_cut_line']['id'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['table'] = 'field_data_field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['field'] = 'field_news_cut_line';
/* Field: Field: Tags */
$handler->display->display_options['fields']['field_news_tags']['id'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['field'] = 'field_news_tags';
$handler->display->display_options['fields']['field_news_tags']['delta_offset'] = '0';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news' => 'news',
);
/* Filter criterion: Content: Photo (field_news_photo:fid) */
$handler->display->display_options['filters']['field_news_photo_fid']['id'] = 'field_news_photo_fid';
$handler->display->display_options['filters']['field_news_photo_fid']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['filters']['field_news_photo_fid']['field'] = 'field_news_photo_fid';
$handler->display->display_options['filters']['field_news_photo_fid']['operator'] = 'not empty';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = '';
$handler->display->display_options['filters']['field_news_tags_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator_id'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['label'] = 'Tags';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['description'] = 'Select one or more existing tags to match';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['identifier'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
/* Filter criterion: Search: Search Terms */
$handler->display->display_options['filters']['keys']['id'] = 'keys';
$handler->display->display_options['filters']['keys']['table'] = 'search_index';
$handler->display->display_options['filters']['keys']['field'] = 'keys';
$handler->display->display_options['filters']['keys']['exposed'] = TRUE;
$handler->display->display_options['filters']['keys']['expose']['operator_id'] = 'keys_op';
$handler->display->display_options['filters']['keys']['expose']['label'] = 'Search Terms';
$handler->display->display_options['filters']['keys']['expose']['description'] = 'Search for specific content';
$handler->display->display_options['filters']['keys']['expose']['operator'] = 'keys_op';
$handler->display->display_options['filters']['keys']['expose']['identifier'] = 'keys';
$handler->display->display_options['filters']['keys']['expose']['remember_roles'] = array(
  2 => '2',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/news/bulk-tag-operations';
