<?php

$view = new view();
$view->name = 'learning_lab_tutor_content';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Learning Lab Tutor Content';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = '<none>';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  7 => '7',
  23 => '23',
  14 => '14',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_learnlab_subject' => 'field_learnlab_subject',
  'field_learnlab_teams_url' => 'field_learnlab_teams_url',
  'title' => 'title',
  'field_learnlab_type' => 'title',
  'field_learnlab_courses' => 'field_learnlab_courses',
  'field_learnlab_schedule' => 'field_learnlab_schedule',
  'field_learnlab_location' => 'field_learnlab_location',
  'field_learnlab_notes' => 'field_learnlab_notes',
  'field_learnlab_tutor_dates' => 'field_learnlab_tutor_dates',
  'status' => 'status',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['class'] = '';
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_learnlab_subject' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_learnlab_teams_url' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '<br>',
    'empty_column' => 0,
  ),
  'field_learnlab_type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_learnlab_courses' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_learnlab_schedule' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_learnlab_location' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_learnlab_notes' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_learnlab_tutor_dates' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Content: Subject */
$handler->display->display_options['fields']['field_learnlab_subject']['id'] = 'field_learnlab_subject';
$handler->display->display_options['fields']['field_learnlab_subject']['table'] = 'field_data_field_learnlab_subject';
$handler->display->display_options['fields']['field_learnlab_subject']['field'] = 'field_learnlab_subject';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Tutor';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Tutor Type */
$handler->display->display_options['fields']['field_learnlab_type']['id'] = 'field_learnlab_type';
$handler->display->display_options['fields']['field_learnlab_type']['table'] = 'field_data_field_learnlab_type';
$handler->display->display_options['fields']['field_learnlab_type']['field'] = 'field_learnlab_type';
$handler->display->display_options['fields']['field_learnlab_type']['label'] = 'Type';
/* Field: Content: Courses Supported */
$handler->display->display_options['fields']['field_learnlab_courses']['id'] = 'field_learnlab_courses';
$handler->display->display_options['fields']['field_learnlab_courses']['table'] = 'field_data_field_learnlab_courses';
$handler->display->display_options['fields']['field_learnlab_courses']['field'] = 'field_learnlab_courses';
/* Field: Location (hidden) */
$handler->display->display_options['fields']['field_learnlab_location']['id'] = 'field_learnlab_location';
$handler->display->display_options['fields']['field_learnlab_location']['table'] = 'field_data_field_learnlab_location';
$handler->display->display_options['fields']['field_learnlab_location']['field'] = 'field_learnlab_location';
$handler->display->display_options['fields']['field_learnlab_location']['ui_name'] = 'Location (hidden)';
$handler->display->display_options['fields']['field_learnlab_location']['label'] = '';
$handler->display->display_options['fields']['field_learnlab_location']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_learnlab_location']['element_label_colon'] = FALSE;
/* Field: Content: Tutor Teams URL */
$handler->display->display_options['fields']['field_learnlab_teams_url']['id'] = 'field_learnlab_teams_url';
$handler->display->display_options['fields']['field_learnlab_teams_url']['table'] = 'field_data_field_learnlab_teams_url';
$handler->display->display_options['fields']['field_learnlab_teams_url']['field'] = 'field_learnlab_teams_url';
$handler->display->display_options['fields']['field_learnlab_teams_url']['label'] = 'Location';
$handler->display->display_options['fields']['field_learnlab_teams_url']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_learnlab_teams_url']['alter']['text'] = '<a href="[field_learnlab_teams_url]">[field_learnlab_location]</a>';
$handler->display->display_options['fields']['field_learnlab_teams_url']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_learnlab_teams_url']['empty'] = '[field_learnlab_location]';
$handler->display->display_options['fields']['field_learnlab_teams_url']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_learnlab_teams_url']['type'] = 'link_plain';
$handler->display->display_options['fields']['field_learnlab_teams_url']['settings'] = array(
  'custom_title' => '',
);
/* Field: Content: Schedule */
$handler->display->display_options['fields']['field_learnlab_schedule']['id'] = 'field_learnlab_schedule';
$handler->display->display_options['fields']['field_learnlab_schedule']['table'] = 'field_data_field_learnlab_schedule';
$handler->display->display_options['fields']['field_learnlab_schedule']['field'] = 'field_learnlab_schedule';
/* Field: Content: Notes */
$handler->display->display_options['fields']['field_learnlab_notes']['id'] = 'field_learnlab_notes';
$handler->display->display_options['fields']['field_learnlab_notes']['table'] = 'field_data_field_learnlab_notes';
$handler->display->display_options['fields']['field_learnlab_notes']['field'] = 'field_learnlab_notes';
/* Field: Content: Tutor Dates */
$handler->display->display_options['fields']['field_learnlab_tutor_dates']['id'] = 'field_learnlab_tutor_dates';
$handler->display->display_options['fields']['field_learnlab_tutor_dates']['table'] = 'field_data_field_learnlab_tutor_dates';
$handler->display->display_options['fields']['field_learnlab_tutor_dates']['field'] = 'field_learnlab_tutor_dates';
$handler->display->display_options['fields']['field_learnlab_tutor_dates']['label'] = 'Start and End Dates';
$handler->display->display_options['fields']['field_learnlab_tutor_dates']['settings'] = array(
  'format_type' => 'short_date_only',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['label'] = 'Published';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
/* Sort criterion: Content: Subject (field_learnlab_subject) */
$handler->display->display_options['sorts']['field_learnlab_subject_value']['id'] = 'field_learnlab_subject_value';
$handler->display->display_options['sorts']['field_learnlab_subject_value']['table'] = 'field_data_field_learnlab_subject';
$handler->display->display_options['sorts']['field_learnlab_subject_value']['field'] = 'field_learnlab_subject_value';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'learning_lab_tutoring_schedule' => 'learning_lab_tutoring_schedule',
);
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published status';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page_canonical');
$handler->display->display_options['path'] = 'admin/workbench/content/tutors';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Tutors';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
