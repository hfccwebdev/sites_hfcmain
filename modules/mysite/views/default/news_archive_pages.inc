<?php

$view = new view();
$view->name = 'news_archive_pages';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'News Archive Pages';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'News Archive';
$handler->display->display_options['css_class'] = 'highlight-header-wide news-cards-grid';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['link_display'] = 'page_6';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
$handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
$handler->display->display_options['exposed_form']['options']['bef'] = array(
  'general' => array(
    'input_required' => 0,
    'text_input_required' => array(
      'text_input_required' => array(
        'value' => 'Select any filter and click on Apply to see results',
        'format' => 'markdown',
      ),
    ),
    'allow_secondary' => 0,
    'secondary_label' => 'Advanced options',
    'secondary_collapse_override' => '0',
  ),
  'body_value' => array(
    'bef_format' => 'default',
    'more_options' => array(
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
  'field_news_tags_tid' => array(
    'bef_format' => 'bef',
    'more_options' => array(
      'bef_select_all_none' => 0,
      'bef_collapsible' => 0,
      'autosubmit' => 0,
      'is_secondary' => 0,
      'any_label' => '',
      'bef_filter_description' => '',
      'tokens' => array(
        'available' => array(
          0 => 'global_types',
          1 => 'vocabulary',
        ),
      ),
      'rewrite' => array(
        'filter_rewrite_values' => '',
      ),
    ),
  ),
);
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '5';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'newsgrid';
/* Relationship: Field: Tags (field_news_tags) */
$handler->display->display_options['relationships']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['relationships']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['relationships']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['relationships']['field_news_tags_tid']['label'] = 'Tags';
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo']['id'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo']['label'] = 'photo only';
$handler->display->display_options['fields']['field_news_photo']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_photo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_photo']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date_1']['id'] = 'field_news_event_date_1';
$handler->display->display_options['fields']['field_news_event_date_1']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date_1']['label'] = 'if photo';
$handler->display->display_options['fields']['field_news_event_date_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date_1']['alter']['text'] = '<div class="event-photo-card">
<div class="event-date">[field_news_event_date_1]</div>
[field_news_photo]
</div>';
$handler->display->display_options['fields']['field_news_event_date_1']['empty'] = '[field_news_photo]';
$handler->display->display_options['fields']['field_news_event_date_1']['settings'] = array(
  'format_type' => 'short_calendar_with_time',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_news_event_date_1']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_news_event_date_1']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_event_date_1']['separator'] = '';
/* Field: Content: Event Date(s) */
$handler->display->display_options['fields']['field_news_event_date']['id'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['table'] = 'field_data_field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['field'] = 'field_news_event_date';
$handler->display->display_options['fields']['field_news_event_date']['label'] = 'if no photo';
$handler->display->display_options['fields']['field_news_event_date']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['alter']['text'] = '<div class="event-no-photo-card">
<div class="event-date">[field_news_event_date]</div>
</div>';
$handler->display->display_options['fields']['field_news_event_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_event_date']['settings'] = array(
  'format_type' => 'short_calendar_with_time',
  'fromto' => 'value',
  'multiple_number' => '1',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_repeat_rule' => 'hide',
);
$handler->display->display_options['fields']['field_news_event_date']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_news_event_date']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_news_event_date']['delta_first_last'] = TRUE;
$handler->display->display_options['fields']['field_news_event_date']['separator'] = ' – ';
/* Field: Field: Paragraphs */
$handler->display->display_options['fields']['field_paragraphs']['id'] = 'field_paragraphs';
$handler->display->display_options['fields']['field_paragraphs']['table'] = 'field_data_field_paragraphs';
$handler->display->display_options['fields']['field_paragraphs']['field'] = 'field_paragraphs';
$handler->display->display_options['fields']['field_paragraphs']['label'] = '';
$handler->display->display_options['fields']['field_paragraphs']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_paragraphs']['alter']['max_length'] = '200';
$handler->display->display_options['fields']['field_paragraphs']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_paragraphs']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['field_paragraphs']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_paragraphs']['settings'] = array(
  'view_mode' => 'full',
);
$handler->display->display_options['fields']['field_paragraphs']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_paragraphs']['separator'] = '. ';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['exclude'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['max_length'] = '200';
$handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['empty'] = '[field_paragraphs]';
/* Field: Content: Title */
$handler->display->display_options['fields']['title_1']['id'] = 'title_1';
$handler->display->display_options['fields']['title_1']['table'] = 'node';
$handler->display->display_options['fields']['title_1']['field'] = 'title';
$handler->display->display_options['fields']['title_1']['label'] = 'read more';
$handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['title_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title_1']['alter']['text'] = '<span class="hidden">[title_1] </span>Read More';
$handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
/* Field: Content: Photo */
$handler->display->display_options['fields']['field_news_photo_1']['id'] = 'field_news_photo_1';
$handler->display->display_options['fields']['field_news_photo_1']['table'] = 'field_data_field_news_photo';
$handler->display->display_options['fields']['field_news_photo_1']['field'] = 'field_news_photo';
$handler->display->display_options['fields']['field_news_photo_1']['label'] = '';
$handler->display->display_options['fields']['field_news_photo_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_photo_1']['alter']['text'] = '[field_news_event_date_1]';
$handler->display->display_options['fields']['field_news_photo_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_photo_1']['empty'] = '[field_news_event_date]';
$handler->display->display_options['fields']['field_news_photo_1']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_news_photo_1']['settings'] = array(
  'image_style' => '',
  'image_link' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: News Summary */
$handler->display->display_options['fields']['field_news_cut_line']['id'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['table'] = 'field_data_field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['field'] = 'field_news_cut_line';
$handler->display->display_options['fields']['field_news_cut_line']['label'] = '';
$handler->display->display_options['fields']['field_news_cut_line']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_cut_line']['alter']['text'] = '<h3>[title]</h3>
[field_news_cut_line]';
$handler->display->display_options['fields']['field_news_cut_line']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_cut_line']['empty'] = '<h3>[title]</h3>
[body]';
/* Field: Content: Offsite Link */
$handler->display->display_options['fields']['field_news_alt_link']['id'] = 'field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['table'] = 'field_data_field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['field'] = 'field_news_alt_link';
$handler->display->display_options['fields']['field_news_alt_link']['label'] = '';
$handler->display->display_options['fields']['field_news_alt_link']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_news_alt_link']['alter']['text'] = '<span class="hidden">[title] </span>Read More';
$handler->display->display_options['fields']['field_news_alt_link']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_news_alt_link']['alter']['path'] = '[field_news_alt_link-url]';
$handler->display->display_options['fields']['field_news_alt_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_news_alt_link']['empty'] = '[title_1]';
$handler->display->display_options['fields']['field_news_alt_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_news_alt_link']['type'] = 'link_plain';
/* Sort criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['sorts']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['sorts']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['sorts']['field_news_release_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
$handler->display->display_options['sorts']['nid']['order'] = 'DESC';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';

/* Display: All News Archive */
$handler = $view->new_display('page', 'All News Archive', 'page_news_all');
$handler->display->display_options['defaults']['footer'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'news_video' => 'news_video',
  'news' => 'news',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Field: Release Date (field_news_release_date) */
$handler->display->display_options['filters']['field_news_release_date_value']['id'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['table'] = 'field_data_field_news_release_date';
$handler->display->display_options['filters']['field_news_release_date_value']['field'] = 'field_news_release_date_value';
$handler->display->display_options['filters']['field_news_release_date_value']['operator'] = '<=';
$handler->display->display_options['filters']['field_news_release_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_news_release_date_value']['default_date'] = 'now';
/* Filter criterion: Field: Tags (field_news_tags) */
$handler->display->display_options['filters']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
$handler->display->display_options['filters']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['value'] = array(
  145 => '145',
  140 => '140',
  141 => '141',
  142 => '142',
  144 => '144',
  143 => '143',
  139 => '139',
);
$handler->display->display_options['filters']['field_news_tags_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator_id'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['label'] = 'Categories';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['operator'] = 'field_news_tags_tid_op';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['identifier'] = 'field_news_tags_tid';
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['remember_roles'] = array(
  2 => '2',
);
$handler->display->display_options['filters']['field_news_tags_tid']['expose']['reduce'] = TRUE;
$handler->display->display_options['filters']['field_news_tags_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_news_tags_tid']['vocabulary'] = 'tags';
$handler->display->display_options['path'] = 'news/archive';
$handler->display->display_options['menu']['title'] = 'Public News';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['title'] = 'News';
$handler->display->display_options['tab_options']['weight'] = '0';

/* Display: Related Page Archive Page */
$handler = $view->new_display('page', 'Related Page Archive Page', 'page_tag_archive');
$handler->display->display_options['display_description'] = 'Filters content by related page nid.';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Field: Related Node (field_related_node) */
$handler->display->display_options['arguments']['field_related_node_target_id']['id'] = 'field_related_node_target_id';
$handler->display->display_options['arguments']['field_related_node_target_id']['table'] = 'field_data_field_related_node';
$handler->display->display_options['arguments']['field_related_node_target_id']['field'] = 'field_related_node_target_id';
$handler->display->display_options['arguments']['field_related_node_target_id']['relationship'] = 'field_news_tags_tid';
$handler->display->display_options['arguments']['field_related_node_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_related_node_target_id']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['field_related_node_target_id']['title'] = 'News Archive for %1';
$handler->display->display_options['arguments']['field_related_node_target_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_related_node_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_related_node_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_related_node_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_related_node_target_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['field_related_node_target_id']['validate']['type'] = 'node';
$handler->display->display_options['path'] = 'news/archive/%';

/* Display: News Tag Related Page */
$handler = $view->new_display('page', 'News Tag Related Page', 'page_by_tid');
$handler->display->display_options['display_description'] = 'Filters content by term tid.';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Tag ID */
$handler->display->display_options['arguments']['tid']['id'] = 'tid';
$handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['arguments']['tid']['field'] = 'tid';
$handler->display->display_options['arguments']['tid']['ui_name'] = 'Tag ID';
$handler->display->display_options['arguments']['tid']['default_action'] = 'not found';
$handler->display->display_options['arguments']['tid']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['tid']['title'] = '%1';
$handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['path'] = 'news/tags/%';
