<?php

$view = new view();
$view->name = 'news_tags_with_related_pages';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'taxonomy_term_data';
$view->human_name = 'News Tags with Related Pages';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'News Tags with Related Pages';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  22 => '22',
  3 => '3',
  17 => '17',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_related_node_target_id']['id'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['table'] = 'field_data_field_related_node';
$handler->display->display_options['relationships']['field_related_node_target_id']['field'] = 'field_related_node_target_id';
$handler->display->display_options['relationships']['field_related_node_target_id']['label'] = 'Related node';
/* Relationship: Taxonomy term: Parent term */
$handler->display->display_options['relationships']['parent']['id'] = 'parent';
$handler->display->display_options['relationships']['parent']['table'] = 'taxonomy_term_hierarchy';
$handler->display->display_options['relationships']['parent']['field'] = 'parent';
/* Field: Taxonomy term: Term ID */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
$handler->display->display_options['fields']['tid']['separator'] = '';
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'Tag Name';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
/* Field: Field: Related Node */
$handler->display->display_options['fields']['field_related_node']['id'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['table'] = 'field_data_field_related_node';
$handler->display->display_options['fields']['field_related_node']['field'] = 'field_related_node';
$handler->display->display_options['fields']['field_related_node']['settings'] = array(
  'bypass_access' => 0,
  'link' => 1,
);
$handler->display->display_options['fields']['field_related_node']['group_rows'] = FALSE;
$handler->display->display_options['fields']['field_related_node']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_related_node']['separator'] = '<br>';
/* Field: Content: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['relationship'] = 'field_related_node_target_id';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['relationship'] = 'field_related_node_target_id';
$handler->display->display_options['fields']['nid']['label'] = 'Archive page';
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['nid']['alter']['text'] = 'view archive';
$handler->display->display_options['fields']['nid']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nid']['alter']['path'] = 'news/archive/[nid]';
/* Sort criterion: Taxonomy term: Weight */
$handler->display->display_options['sorts']['weight_1']['id'] = 'weight_1';
$handler->display->display_options['sorts']['weight_1']['table'] = 'taxonomy_term_data';
$handler->display->display_options['sorts']['weight_1']['field'] = 'weight';
$handler->display->display_options['sorts']['weight_1']['relationship'] = 'parent';
/* Sort criterion: Taxonomy term: Weight */
$handler->display->display_options['sorts']['weight']['id'] = 'weight';
$handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
$handler->display->display_options['sorts']['weight']['field'] = 'weight';
/* Filter criterion: Taxonomy vocabulary: Machine name */
$handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
$handler->display->display_options['filters']['machine_name']['value'] = array(
  'tags' => 'tags',
);
/* Filter criterion: Field: Related Node (field_related_node) */
$handler->display->display_options['filters']['field_related_node_target_id']['id'] = 'field_related_node_target_id';
$handler->display->display_options['filters']['field_related_node_target_id']['table'] = 'field_data_field_related_node';
$handler->display->display_options['filters']['field_related_node_target_id']['field'] = 'field_related_node_target_id';
$handler->display->display_options['filters']['field_related_node_target_id']['operator'] = 'not empty';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/news/related';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Related Pages';
$handler->display->display_options['menu']['weight'] = '';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
