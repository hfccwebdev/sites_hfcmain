<?php

/**
 * @file
 * Views handlers for mysite.module
 */

/**
 * Implements hook_views_data_alter().
 */
function mysite_views_data_alter(&$data) {

  // if field_news_alt_link is defined on this site, add a
  // views field handler to generate URL for news views
  // based on whether or not this field is populated.
  $field_info = field_info_field('field_news_alt_link');
  if (!empty($field_info)) {
    $data['field_data_field_news_alt_link']['news_url'] = [
      'group' => t('Content'),
      'title' => t('News URL'),
      'help' => t("Use the alt link for URL if it exists."),
      'real field' => 'field_news_alt_link_url',
      'field' => [
        'handler' => 'mysite_handler_field_mysite_news_url',
        'click sortable' => FALSE,
      ],
    ];
    $data['field_data_field_news_event_date']['field_news_event_ending_timestamp'] = [
      'group' => t('Content'),
      'title' => t('Event End Timestamp'),
      'help' => t("Compare against the ending timestamp."),
      'real field' => 'field_news_event_date_value2',
      'filter' => [
        'handler' => 'mysite_handler_field_news_event_timestamp',
      ],
    ];
    $data['field_data_field_news_retire_date']['field_news_retire_ending_timestamp'] = [
      'group' => t('Content'),
      'title' => t('Retire Date Timestamp'),
      'help' => t("Compare against the ending timestamp."),
      'real field' => 'field_news_retire_date_value',
      'filter' => [
        'handler' => 'mysite_handler_field_news_event_timestamp',
      ],
    ];
    $data['views_entity_node']['retire_news'] = [
      'field' => [
        'title' => t('Link to retire news content.'),
        'help' => t('Provide a link to news retire form.'),
        'handler' => 'mysite_handler_news_retire',
      ],
    ];
  }
}
