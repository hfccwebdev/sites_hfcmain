<?php

/**
 * @file
 * Contains drush commands provided by this module.
 */

/**
 * Implements hook_drush_command().
 */
function mysite_drush_command() {
  return [
    'missing-catalog-programs' => [
      'description' => 'Scan for missing Catalog Program references.',
      'aliases' => ['mcp'],
    ],
    'employee-directory-check' => [
      'description' => 'Scan for updated Employee Directory info on employee content.',
    ],
  ];
}

/**
 * Callback for the drush media-archive-scan-all command.
 */
function drush_mysite_missing_catalog_programs() {
  echo SiteDataIntegrityTools::create()->missingProgramsCheck();
  echo SiteDataIntegrityTools::create()->missingCoursesCheck();
}

/**
 * Callback for the drush employee-directory-check command.
 */
function drush_mysite_employee_directory_check() {
  echo SiteDataIntegrityTools::create()->employeeMismatchReport();
}
