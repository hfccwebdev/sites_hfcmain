<?php

/**
 * @file
 * This file provides theme override functions for the hfcmain subtheme.
 */

/**
 * Implements hook_template_preprocess_page().
 *
 * Preprocess variables for page.tpl.php.
 */
function hfcmain_preprocess_page(&$variables) {
  if (@$variables['node']->type == 'tuition_compare') {
    $title = t('Compare HFC Tuition to @title', array('@title' => $variables['node']->title));
    drupal_set_title($title, PASS_THROUGH);
  }
}
