<?php

/**
 * @file
 * Theme implementation to display program_para nodes in "newsgrid" format.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($content['field_promo_type']); ?>
  <?php print render($title_prefix); ?>
  <a class="newsgrid-title-link" href="<?php print $node_url; ?>">
    <?php if (!empty($article_cover_image)): ?>
      <img class="article-cover-image" src="<?php print $article_cover_image; ?>" <?php print $article_cover_image_attributes; ?>>
    <?php endif; ?>
    <h3><?php print $title; ?></h3>
  </a>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['field_promo_type']);
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>
</div>
