<?php

/**
 * @file
 * This file provides theme override functions for the hfcmain18 subtheme.
 */

/**
 * Implements hook_template_preprocess_page().
 *
 * Preprocess variables for page.tpl.php.
 */
function hfcmain18_preprocess_page(&$variables) {
  if (@$variables['node']->type == 'tuition_compare') {
    $title = t('Compare HFC Tuition to @title', array('@title' => $variables['node']->title));
    drupal_set_title($title, PASS_THROUGH);
  }
}

/**
 * Implements hook_page_alter().
 */
function hfcmain18_page_alter(&$variables) {
  if ($node = menu_get_object('node')) {
    if (in_array($node->type, ['news', 'program_para', 'presidents_communications'])) {
      $variables['#notitle'] = TRUE;
    }
    elseif (
      !empty($node->field_header_image[LANGUAGE_NONE][0]['fid']) &&
      empty($node->field_header_image[LANGUAGE_NONE][0]['is_default'])
    ) {
      $variables['#notitle'] = TRUE;
    }
  }
}

/**
 * Implements HOOK_preprocess_hook().
 */
function hfcmain18_preprocess_node(&$variables) {

  $node = $variables['node'];
  $mode = $variables['view_mode'];

  if (in_array($mode,['newsgrid', 'feature', 'featurebg'])) {
    $variables['theme_hook_suggestions'][] = "node__{$node->type}__$mode";
    $variables['classes_array'][] = "node-news-$mode";
    $variables['classes_array'][] = drupal_html_class("node--{$node->type}--$mode");

    if (!empty($node->field_news_cover_image)) {
      $image = file_create_url($node->field_news_cover_image[LANGUAGE_NONE][0]['uri']);
      $image_attributes = _hfcmain18_image_attributes($node->field_news_cover_image);
    }
    elseif (!empty($node->field_news_photo)) {
      $image = file_create_url($node->field_news_photo[LANGUAGE_NONE][0]['uri']);
      $image_attributes = _hfcmain18_image_attributes($node->field_news_photo);
    }
    elseif (!empty($node->field_photo)) {
      $image = file_create_url($node->field_photo[LANGUAGE_NONE][0]['uri']);
      $image_attributes = _hfcmain18_image_attributes($node->field_photo);
    }
    else {
      $image = file_create_url(drupal_get_path('theme', 'hfcmain18') . '/images/hfc-box.jpg');
      $image_attributes = [
        'alt' => t('Placeholder image'),
        'title' => t('Placeholder image'),
      ];
      $image_attributes = drupal_attributes($image_attributes);
    }

    if (isset($image)) {
      $variables['classes_array'][] = "$mode-has-background-image";
      $variables['article_cover_image'] = $image;
      $variables['article_cover_image_attributes'] = $image_attributes;

      if ($mode == 'featurebg') {
        $variables['attributes_array']['style'] = "background-image: url($image)";
      }
    }
    else {
      $variables['classes_array'][] = "$mode-no-background-image";
    }
    if (!empty($node->field_news_event_date)) {
      $variables['classes_array'][] = "$mode-has-event-date";

      $tools = MysiteTools::create();
      $variables['event_date'] = $tools->formatNewsGridEventDates($node->field_news_event_date[LANGUAGE_NONE]);
    }
  }

  if (!empty($node->field_news_alt_link)) {
    $variables['node_url'] = $node->field_news_alt_link[LANGUAGE_NONE][0]['url'];
  }

  if ($node->type == 'news' && $mode == 'full') {
    $variables['theme_hook_suggestions'][] = "node__news__full";
    $variables['classes_array'][] = 'node-news-full';
    $variables['news_hide_fields'] = !empty($node->field_hide_photo[LANGUAGE_NONE][0]['value'])
      ? $node->field_hide_photo[LANGUAGE_NONE][0]['value']
      : NULL;
    $variables['news_photo_caption'] = !empty($node->field_news_photo[LANGUAGE_NONE][0]['title'])
      ? check_plain($node->field_news_photo[LANGUAGE_NONE][0]['title'])
      : NULL;
    if (!empty($variables['content']['field_news_event_date'])) {
      $variables['content']['field_news_event_date']['#title'] = t('Event Date');
    }
  }

  if ($node->type == 'program_para') {
    $variables['apply_link_url'] = $node->field_program_apply_link[LANGUAGE_NONE][0]['original_url'] ?? '/apply';
    $variables['apply_link_text'] = t('Apply to HFC');
  }

  if (!empty($node->field_header_image[LANGUAGE_NONE][0]['fid'])) {
    $path = file_create_url($node->field_header_image[LANGUAGE_NONE][0]['uri']);
    $inline_style = "background-image: url($path)";
    $variables['header_background_image'] = $inline_style;
  }
}

/**
 * Get alt and title tags for news cover images.
 */
function _hfcmain18_image_attributes($field) {
  $attributes = [];

  if (!empty($field[LANGUAGE_NONE][0]['alt'])) {
    $attributes['alt'] = $field[LANGUAGE_NONE][0]['alt'];
  }
  elseif (!empty($field[LANGUAGE_NONE][0]['title'])) {
    $attributes['alt'] = $field[LANGUAGE_NONE][0]['title'];
  }

  if (!empty($field[LANGUAGE_NONE][0]['title'])) {
    $attributes['title'] = $field[LANGUAGE_NONE][0]['title'];
  }
  elseif (!empty($field[LANGUAGE_NONE][0]['alt'])) {
    $attributes['title'] = $field[LANGUAGE_NONE][0]['alt'];
  }

  return drupal_attributes($attributes);
}

/**
 * Implements hook_template_preprocess_entity().
 */
function hfcmain18_preprocess_entity(&$variables) {
  if ($variables['entity_type'] == 'paragraphs_item') {
    $entity = $variables['elements']['#entity'];
    if ($entity->bundle == 'news_tag_list') {
      $tids = array_map(function ($tag) { return $tag['tid']; }, $variables['field_news_tags']);
      $year = $variables['field_news_release_year'][0]['value'];
      $variables['content'] = MysiteTools::create()->getTermContentByYear($tids, $year);
    }
  }
}

/**
 * Implements HOOK_preprocess_hook().
 */
function hfcmain18_preprocess_field(&$variables) {

  if ($variables['element']['#field_name'] == 'field_contact_person') {
    foreach ($variables['items'] as $delta => $item) {
      $variables['items'][$delta]['#element']['attributes']['class'] = 'program-contact-link';
    }
  }
  if ($variables['element']['#field_name'] == 'field_resource_link') {
    foreach ($variables['items'] as $delta => $item) {
      $variables['items'][$delta]['#element']['attributes']['class'] = 'program-resource-link';
    }
  }
  if ($variables['element']['#field_name'] == 'field_office_multiple') {
    foreach ($variables['items'] as $delta => $item) {
      $variables['items'][$delta]['#display_hours'] =
        WebServicesClient::getOfficeHoursToday($item) ??
        $item['note'];
    }
  }
}
