// Pause and play the video, and change the button text
function toggleVideoBackground() {
  var video = document.getElementById("videoBackground");
  var btn = document.getElementById("videoBackgroundButton");
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}
